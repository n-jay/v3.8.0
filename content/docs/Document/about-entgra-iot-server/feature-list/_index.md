---
bookCollapseSection: true
weight: 9
---
# Feature List


Entgra EMM server contains large number of features to cater to every enterprise mobility need of the customers. Features include and not limited to mobile device management, mobile application management, Apple DEP integration, Android enterprise integrations, containerization support and much more. Following operating systems are supported,

<div>
    <ul style="list-style-type:disc;">
        <li><a href="android">Android</a></li>
        <li><a href="ios">iOS & MacOS</a></li>
        <li><a href="windows">Windows</a></li>
    </ul>
</div>
