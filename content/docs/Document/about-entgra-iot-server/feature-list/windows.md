---
bookCollapseSection: true
weight: 5
---

# Operations

Following operations can be executed on a Windows device.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Description</th></tr></thead><tbody>
 <tr><td>Get Device Information</td><td>Fetch the device's runtime information.</td></tr>
 <tr><td>Get Installed Applications</td><td>Fetch the device's current location</td></tr>
 <tr><td>Wipe Device(factory reset)</td><td>Wipe the entreprise portion of the device</td></tr>
 <tr><td>Lock Device</td><td>Lock the device remotely. Similar to pressing the power button on the device and locking it.</td></tr>
 <tr><td>Ring Device</td><td>Ring the device for the purpose of locating the device in case of misplace.</td></tr>
 <tr><td>Clear Passcode</td><td>Changes the device's currently set lock code. From Android N upwards, clear passcode will not work</td></tr>
</tbody></table>

# Policies

Following policies can be executed on a Windows device.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Description</th></tr></thead><tbody>
 <tr><td>Passcode policy</td><td>Define a password policy for the devices.</td></tr>
 <tr><td>Encrypt storage</td><td>Encrypt data on the device, when the device is locked and make it readable when the passcode is entered.</td></tr>
</tbody></table>

## Restriction policy

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Description</th></tr></thead><tbody>
 <tr><td>System/AllowLocation</td><td>Disable or enable location services</td></tr>
 <tr><td>System/AllowUserToResetPhone</td><td>Control acces to factory reset</td></tr>
 <tr><td>DisableOneDriveFileSync</td><td>Disable or enable syncing files with One drive storage</td></tr>
 <tr><td>System/DisableSystemRestore</td><td>Disable or enable system restore capabilities</td></tr>
 <tr><td>System/AllowStorageCard</td><td>Restrict access to external storages such as SD cards or USB</td></tr>
 <tr><td>Security/AllowManualRootCertificateInstallation</td><td>Allow installing root certificates and intermidiate certificates</td></tr>
 <tr><td>Connectivity/AllowBluetooth</td><td>Disable or enable Bluetooth</td></tr>
 <tr><td>Connectivity/AllowCellularData</td><td>Disable or enable mobile data</td></tr>
 <tr><td>Connectivity/AllowCellularDataRoaming</td><td>Disable or enable data roaming</td></tr>
 <tr><td>Connectivity/AllowConnectedDevices</td><td>Disable or enable Connected Devices Platform (CDP) which allows to discover connected device</td></tr>
 <tr><td>Connectivity/AllowNFC</td><td>Disable or enable NFC beams</td></tr>
 <tr><td>Connectivity/AllowPhonePCLinking</td><td>Disable or enable the ability to connect to perform a continious task with a link between phone and PC</td></tr>
 <tr><td>Connectivity/AllowUSBConnection</td><td>Disable or enable USB connection</td></tr>
 <tr><td>Connectivity/AllowVPNOverCellular</td><td>Decides if a VPN can be created over mobile data</td></tr>
 <tr><td>Connectivity/AllowVPNRoamingOverCellular</td><td>Decides if a VPN can be created over mobile data while roaming</td></tr>
 <tr><td>Disable adding non-microsoft accounts</td><td>Disable adding non Microsoft based account to the device</td></tr>
 <tr><td>disable private browsing</td><td>Decides if private browsing is allowed on the device </td></tr>
 <tr><td>disable removable drive indexing</td><td>Decides if the search results contain files from removable devices</td></tr>
 <tr><td>disable language</td><td>Disables language settings</td></tr>
 <tr><td>disable cortana </td><td>Decides if cortana is allowed on the device</td></tr>
 <tr><td>disable region</td><td>Disables region settings</td></tr>
 <tr><td>disble date and time</td><td>Disable changing date and time settings </td></tr>
 <tr><td>assigned access</td><td>Lock a user to a single application</td></tr>
</tbody></table>

