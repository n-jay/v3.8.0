
# Advanced Device Search

This section provides the details on how the advanced device search functions. Entgra IoT Server maintains the details of the devices that are enrolled with it by running a background task. This task adds a few operations to the devices such as the `device_info`, `device_location` and the `application_list`. Further, you can add more device operations and execute the advanced search as explained below:



Prerequisite



This feature is only available on the **Entgra IoT Server 3.10-Update1** pack. Download the [Entgra IoT Server 3.10-Update1](https://github.com/wso2/product-iots/releases/tag/v3.1.0-update1) pack to try it out.





1.  Navigate to the respective device types XML file in `<IOTS_HOME>/repository/deployment/server/devicetypes/<DEVICE_TYPE>.xml` file.

2.  Add the new operations under the task configurations operations tag as shown below:

    
    <TaskConfiguration>
       <Enable>true</Enable>
       <Frequency>600000</Frequency>
       <TaskClass>org.wso2.carbon.device.mgt.core.task.impl.DeviceDetailsRetrieverTask</TaskClass>
       <Operations>
          <Operation>
             <Name>{OPERATION_CODE}</Name>
             <RecurrentTimes>{HOW_OFTEN_THE_OPERATION_NEEDS_TO_BE_ADDED}</RecurrentTimes>
          </Operation>
       </Operations>
    </TaskConfiguration>

    

    

    For `<RecurrentTimes>` define how often the above operation should be called when running the task operation

    Example: If you define it as 5 that means the operation will be added to the task at every 5th run of the task.

    

    

    Example:

    <TaskConfiguration>
       <Enable>true</Enable>
       <Frequency>30000</Frequency>
       <TaskClass>org.wso2.carbon.device.mgt.core.task.impl.DeviceDetailsRetrieverTask</TaskClass>
       <Operations>
          <Operation>
             <Name>DEVICE_INFO</Name>
             <RecurrentTimes>1</RecurrentTimes>
          </Operation>
          <Operation>
             <Name>APPLICATION_LIST</Name>
             <RecurrentTimes>5</RecurrentTimes>
          </Operation>
          <Operation>
             <Name>DEVICE_LOCATION</Name>
             <RecurrentTimes>1</RecurrentTimes>
          </Operation>
       </Operations>
    </TaskConfiguration>

3.  The user executes the advanced search for devices via the Entgra device management console or the REST API. 

    

    

    

    

    *   To search for devices via the location, enter the location details and click **Search**.
    *   To search using other options, click **Add a custom search parameter, **enter the required fields, and click **Search**.

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <thead>
            <tr>
              <th>Field</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>State</td>
              <td>There can be many search options as shown in the sample JSON definition. The field that connects the independent search is known as&nbsp;<code>state</code>.&nbsp;The following values can be assigned to state:<ul>
                  <li><strong><code>AND</code> </strong>&nbsp;: Defines if you want the search result has to match all the search conditions provided.</li>
                  <li><strong><code>OR</code> </strong>&nbsp;: Defines if you want the search result to match either of the search conditions provided.</li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>Key</td>
              <td>The feature code. You can assign the following feature codes as the value for key:<ul>
                  <li>
                    <p><strong><code>DEVICE_MODEL</code> </strong>: The model of the device.</p>
                  </li>
                  <li>
                    <p><strong><code>VENDOR</code> </strong>: The name of the&nbsp;Android device&nbsp;vendor.</p>
                  </li>
                  <li>
                    <p><strong><code>OS_VERSION</code> </strong>: The version of the device operating system.</p>
                  </li>
                  <li>
                    <p><strong><code>BATTERY_LEVEL</code> </strong>: The current level of the&nbsp;device&nbsp;battery.</p>
                  </li>
                  <li>
                    <p><strong><code>INTERNAL_TOTAL_MEMORY</code> </strong>: The total capacity of the internal memory available in the device.</p>
                  </li>
                  <li>
                    <p><strong><code>INTERNAL_AVAILABLE_MEMORY</code> </strong>:&nbsp;The unutilized internal memory capacity in the device.</p>
                  </li>
                  <li>
                    <p><strong><code>EXTERNAL_TOTAL_MEMORY</code> </strong>: The total capacity of the external memory available in the device.</p>
                  </li>
                  <li>
                    <p><strong><code>EXTERNAL_AVAILABLE_MEMORY</code> </strong>: The unutilized external memory capacity in the device.</p>
                  </li>
                  <li>
                    <p><strong><code>CONNECTION_TYPE</code> </strong>:</p>
                  </li>
                  <li>
                    <p><strong><code>SSID</code> </strong>: The name of the Wifi network that the device is connected to.</p>
                  </li>
                  <li>
                    <p><strong><code>CPU_USAGE</code> </strong>: The current CPU usage of the mobile device.<span style="font-family: monospace;">&nbsp;</span></p>
                  </li>
                  <li><strong style="font-family: monospace;">TOTAL_RAM_MEMORY</strong>:&nbsp;The total capacity of the random access memory available in the device.</li>
                  <li><code><strong>AVAILABLE_RAM_MEMORY</strong></code>:&nbsp;The unutilized random access memory capacity available in the device.</li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>Value</td>
              <td>
                <p>Define the value for the key you provide.</p>
                <p>Example: If you provide the key as<strong> <code>VERSION</code> </strong>, you can provide the value as&nbsp;<strong> <code>5.1</code> </strong>, which indicates the version of the mobile device you are searching.</p>
              </td>
            </tr>
            <tr>
              <td>Operator</td>
              <td>
                <p>Define the search condition between the key and the value you provide. The following values can be used to define the search condition:</p>
                <ul>
                  <li><strong><code>=&nbsp;</code> </strong>: Searches for devices where the key equals the value.</li>
                  <li><strong><code>=!&nbsp;</code> </strong>: Searches for devices where the key is not equal to the value.</li>
                  <li><strong><code>&lt;=&nbsp;</code> </strong>: Searches for devices where the key is greater than or equal to the value.</li>
                  <li><strong><code>&gt;=&nbsp;</code> </strong>: Searches for devices where the key is less than or equal to the value.</li>
                  <li><strong><code>&gt;&nbsp;</code> </strong>: Searches for devices where the key is greater than the value.</li>
                  <li><strong><code>&lt;&nbsp;</code> </strong>: Searches for devices where the key is less than the value.</li>
                </ul>
                <p>Example: If you wish to get the devices that have the version as 5.1, you need to use the&nbsp;<code>=</code>&nbsp;o</p>
              </td>
            </tr>
          </tbody>
        </table>

    

    

    ![image](352824861.png)

    

4.  Entgra IoTS goes through the search request received via the console or the REST API to check for the following details:
    *   Checks if the values provided for **Key** field match the `D` `EVICE_DETAILS` database table properties. 

        

        

        

        *   `DEVICE_MODEL`

        *   `VENDOR`

        *   `OS_VERSION`

        *   `BATTERY_LEVEL`

        *   `INTERNAL_TOTAL_MEMORY`

        *   `INTERNAL_AVAILABLE_MEMORY`

        *   `EXTERNAL_TOTAL_MEMORY`

        *   `EXTERNAL_AVAILABLE_MEMORY`

        *   `CONNECTION_TYPE`

        *   `SSID`

        *   `CPU_USAGE`

        *   `TOTAL_RAM_MEMORY`

        *   `AVAILABLE_RAM_MEMORY`

        *   `PLUGGED_IN`

        

        

    *   If the value you defined for the **Key** field is not a property in the `D` `EVICE_DETAILS` table, it will then form a `key:value` pair and pass the details to the `D` `EVICE_INFO` database table in Entgra IoTS.
5.  Once Entgra IoTS aggregates all the results for the given search query the final search result will be provided.
