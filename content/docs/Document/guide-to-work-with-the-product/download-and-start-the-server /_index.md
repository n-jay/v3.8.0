---
bookCollapseSection: true
weight: 1
---
# Download and start the server
This section explains how to do some basic configurations and start up the server. Before proceeding, make sure to read the <a href="system-requirements">system requirements</a> page and have an environnement ready.
This guide uses a linux based OS.


## Step 1
Go to [Entgra IoT Server](http://wso2.com/iot)