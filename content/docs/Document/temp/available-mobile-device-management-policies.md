---
bookCollapseSection: true
weight: 6
---

# Available Mobile Device Management Policies

## Table of contents


We identified a set of policies to help manage mobile devices such as Android, iOS, and Windows. These policies are available by default on [Entgra IoT Server](http://entgra.io/). For more information on how to add a policy on mobile devices, see [Adding a Mobile Device Management Policy](#available-mobile-device-management-policies). 

The policies available for mobile device management are listed below:

<!-- <div class="panelContent" style="background-color: #ffffff;"> -->

### Policies for Android devices

The mobile device management administrator can [add a new policy](about:blank#) to a preferred device type, such as BYOD, or COPE. The following policies are available for the Android platform.

<table width="100%">
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Policy</th>
      <th>Description</th>
    </tr>
    <tr>
      <th>Passcode policy</th>
      <td>Define a password policy for the devices.</td>
    </tr>
    <tr>
      <th>
        <div class="content-wrapper"><span class="confluence-anchor-link" id="AvailableMobileDeviceManagementPolicies-restrictions"></span>Restrictions</div>
      </th>
      <td>
        <div class="content-wrapper">
          <p>Allow or disallow users from using the following features on Android devices. Most of the<span> restrictions require the</span> <a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/tutorials/android/setting-up-work-profile/" data-linked-resource-id="336563346" data-linked-resource-version="1" data-linked-resource-type="page">Android work profile to be set up</a><span>,</span> <a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application" data-linked-resource-id="336569881" data-linked-resource-version="1" data-linked-resource-type="page">system app installed</a> <span>or for the </span><a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#setting-up-single-purpose-devices" data-linked-resource-id="336570043" data-linked-resource-version="1" data-linked-resource-type="page">device to be a single-purpose device</a><span>.</span></p>
          <div class="confluence-information-macro confluence-information-macro-note"><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
            <div class="confluence-information-macro-body">
              <p><span style="color: rgb(38,50,56);"><span style="color: rgb(38,50,56);">Please note that the restrictions mentioned under the device ownership application do not work for Samsung devices</span><span style="color: rgb(38,50,56);"><span>&nbsp;</span>with Android 6.0 or below. However you can apply those restrictions on Samsung devices with Android 7.0 or higher, whenever</span><span style="color: rgb(23,43,77);">&nbsp;Samsung KNOX, Reactivation lock or Samsung account is not configured in the device.</span></span></p>
            </div>
          </div>
          <div class="table-wrap">
            <table width="100%">
              <tbody>
                <tr>
                  <th>Restrictions</th>
                  <th>All device types</th>
                  <th><a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/tutorials/android/setting-up-work-profile/" data-linked-resource-id="336563346" data-linked-resource-version="1" data-linked-resource-type="page">Require the<br>Android work<br>profile</a></th>
                  <th><a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application" data-linked-resource-id="336569881" data-linked-resource-version="1" data-linked-resource-type="page">Require the<br>system app<br></a>or<br><a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#device-ownership-application" data-linked-resource-id="336569920" data-linked-resource-version="2" data-linked-resource-type="page">the device<br>ownership application</a></th>
                  <th><a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#setting-up-single-purpose-devices" data-linked-resource-id="336570043" data-linked-resource-version="1" data-linked-resource-type="page">Require the<br>single-purpose<br>device</a></th>
                </tr>
                <tr>
                  <td>
                    <p>Using the camera on the device.</p>
                  </td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Configuring user credentials.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Configuring VPN.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Restricting items copied to the clipboard from being posted on related profiles.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Enabling or accessing debugging features.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Installing applications.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Enabling the "Unknown Sources" setting.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <div class="content-wrapper">
                      <p><span><span><span>Adding and removing accounts unless they are </span><span>programmatically</span><span>&nbsp;added by the Authenticator. </span><span>For more information, see&nbsp;</span><a class="external-link" href="http://developer.android.com/intl/zh-cn/reference/android/accounts/AccountManager.html#addAccountExplicitly(android.accounts.Account, java.lang.String, android.os.Bundle)" rel="nofollow">the details on adding an account directly</a><span>.</span></span></span></p>
                      <div class="confluence-information-macro confluence-information-macro-note">
                        <p class="title">Note!</p><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
                        <div class="confluence-information-macro-body">
                          <p>If you enabled this policy before configuring the Google Play Store with a Google Account, the following message is displayed when you open the Google Play app: <strong>“This change is not allowed by the device administrator”</strong>.</p>
                          <p>To use the Google Play Store with this policy, you need to set up the Google account and configure Google Play before applying this policy.</p>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Restrict the use of Near Filed Communication (NFC) to beam out data from apps.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Turning on location sharing.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Uninstalling applications.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Allows or disallow apps in the parent profile to handle web links from the managed profile.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Disabling application verification</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td><span>Enabling the auto time feature in the device that is in&nbsp;</span> <strong>Settings &gt; Date &amp; Time</strong><span>.</span></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Disabling the screen shot option on the device.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Restricting the user from sending or receiving Short Message Service (SMS )messages.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Adjusting the master volume.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Configuring cell broadcasts.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>C<span>onfiguring Bluetooth</span></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Configuring mobile networks.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td><span>Transferring files over USB.</span></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Changing Wi-Fi access.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Device rebooting.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Making outgoing phone calls.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Mounting physical external media.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td><span>Restricting windows beside the app window from being created.</span></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Factory resetting the device from <strong>Settings</strong>.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Removing other users.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Adding new users and profiles.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Resetting the network settings from <strong>Settings</strong>.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td><span>Transferring files over USB.</span></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <p>Adjusting the microphone volume.</p>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
                <tr>
                  <td>
                    <div class="content-wrapper">
                      <p>Disabling the status bar on the device.</p>
                      <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
                        <div class="confluence-information-macro-body">
                          <p>This restriction is only supported in <a rel="nofollow">Android version 6.0 Marshmallow and higher.</a></p>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="red"><br></td>
                  <td data-highlight-colour="green"><br></td>
                  <td data-highlight-colour="green"><br></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </td>
    </tr>
    <tr>
      <th>Encrypt storage</th>
      <td>Encrypt data on the device, when the device is locked and make it readable when the passcode is entered.</td>
    </tr>
    <tr>
      <th>
        <p class="p1">Wi-Fi</p>
      </th>
      <td>Ability to configure the Wi-Fi access on a device. Entgra IoT Server provides advanced Wi-Fi configuration settings, as shown below:<br>
        <ul>
          <li>You are able to configure the Wi-Fi settings for the&nbsp;<code>WEP</code>,&nbsp;<code>WPA/WPS 2PSK</code>&nbsp;and&nbsp;<code>802.1 EAP</code>&nbsp;security types.&nbsp;</li>
          <li>The&nbsp;<code>802.1 EAP</code>&nbsp;security type works only for Android 4.3 and above.</li>
          <li>Entgra IoT Server supports the following EAP methods:&nbsp;<code>PEAP</code>,&nbsp;<code>TLS</code>,&nbsp;<code>TTLS</code>,&nbsp;<code>PWD</code>,&nbsp;<code>SIM</code>, and&nbsp;<code>AKA</code>.</li>
          <li>If you want to provide the identity of the user that access the Wi-Fi through their Android device, you can provide&nbsp;<code>[user]</code>&nbsp;as the value for&nbsp;<strong>Identity</strong>&nbsp;and it will provide the username used by the user to enroll their Android device with Entgra IoT Server. This setting is only applicable for the following EAP methods:&nbsp;<code>PEAP</code>,&nbsp;<code>TLS</code>,&nbsp;<code>TTLS</code>, and&nbsp;<code>PWD</code>.</li>
        </ul>&nbsp;
      </td>
    </tr>
    <tr>
      <th>VPN</th>
      <td>Ability to specify the VPN and per app VPN settings.</td>
    </tr>
    <tr>
      <th>Work-Profile Configurations</th>
      <td>
        <div class="content-wrapper">
          <p>Ability separate the personal and work related data on your device via the managed profile feature.</p>
          <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
            <div class="confluence-information-macro-body">
              <p>For more information on how it works, see <a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#data-containerization-for-android-device" data-linked-resource-id="336569900" data-linked-resource-version="1" data-linked-resource-type="page">Data Containerization for Android Device</a>.</p>
            </div>
          </div>
        </div>
      </td>
    </tr>
    <tr>
      <th>
        <div class="content-wrapper"><span class="confluence-anchor-link" id="AvailableMobileDeviceManagementPolicies-blacklisting"></span>Application restrictions</div>
      </th>
      <td>
        <div class="content-wrapper">
          <p>Ability blacklist and whitelist applications on the Android platform. Let's take a look at how it works:</p>
          <p><strong>Blacklist applications</strong></p>
          <p>Prevents you from using the applications defined in the policy. For Android operation systems before Lollipop, when a blacklisted application is clicked a screen is displayed to prevent you from using the app. For the Lollipop Android operating systems and after, the blacklisted apps will be hidden. Blacklisting can be used on both BYOD and COPE devices.</p>
          <p><strong>Whitelisting applications</strong></p>
          <p>Allows you to only install the applications defined in the policy. This feature requires another application, i.e., Entgra IoT Server System app, that is signed by the device firmware owner. Therefore, generally, it will be available for COPE devices but if you are able to get the Entgra IoT Server system application signed via a firmware signing key, then you are able to use it for BYOD devices too.</p>
          <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
            <div class="confluence-information-macro-body">
              <p>In addition to the above, you are able to enable application restrictions via the <a href="https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=28278926#AvailableMobileDeviceManagementPolicies-restrictions" data-linked-resource-id="27459929" data-linked-resource-version="1" data-linked-resource-type="page">restrictions policy</a>. The restrictions policy has two settings to restrict application installation and uninstallation. For this, the Entgra IoT Server application needs to have device owner privileges or the device needs to have the Entgra IoT Server System app installed.</p>
            </div>
          </div>
        </div>
      </td>
    </tr>
  </tbody>
</table>

<!-- </div> -->

<!-- <div class="panelContent" style="background-color: #ffffff;"> -->

### Policies for iOS devices

The mobile device management administrator is able to restrict operations on Windows devices by [adding a new policy](about:blank#). The following policies are available for the iOS platform.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Policies</th>
      <th>Description</th>
    </tr>
    <tr>
      <th>
        <p class="p1">Passcode policy</p>
      </th>
      <td>Define a password policy for the devices.</td>
    </tr>
    <tr>
      <th>
        <div class="content-wrapper">
          <p>Restrictions</p>
          <p><span class="confluence-anchor-link" id="AvailableMobileDeviceManagementPolicies-ios-restrictions"></span></p>
        </div>
      </th>
      <td>
        <p>Restricts the usage of the camera and other functions.</p>
        <p><span>You are able to allow or disallow users from using the following features on the device:</span></p>
        <ul>
          <li>Restrict users from installing applications on the device.</li>
          <li>Prohibit users from adding friends to the Game Center.</li>
          <li>Restrict users from removing applications from the device.</li>
          <li>Restrict users from using Siri.</li>
          <li>Prevent Siri from querying user-generated content from the web.</li>
          <li>
            <p>Prevent users from using Siri when the device is locked. Availability: iOS 5.1 and later.&nbsp;</p>
          </li>
          <li>Restrict users from using the camera. If this operation is not allowed the camera icon will be removed from the home screen.</li>
          <li>
            <p>Prevent users from backing up the device data to iCloud. <span>Availability: iOS 5.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Disable documents and key-value syncing to iCloud. <span>Availability: iOS 5.0 and later.</span></p>
          </li>
          <li>
            <p>Disable Cloud keychain synchronization. <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Prevent the device from automatically submitting diagnostic reports to Apple. <span>Availability: Only in iOS 6.0 and later.&nbsp;</span></p>
          </li>
          <li>Hide explicit music or video content purchased from the iTunes Store. Explicit content is marked by content providers, such as record labels,&nbsp;when sold through the iTunes Store.&nbsp;</li>
          <li>
            <p>Prevent the Touch ID from unlocking a device. <span>Availability: iOS 7 and later.&nbsp;</span></p>
          </li>
          <li>Disable the global background fetch activity when an iOS phone is on roaming.</li>
          <li>Prohibit in-app purchasing.</li>
          <li>
            <p>Prevent the Control Center from appearing on the Lock screen. <span>Availability: iOS 7 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Disable host pairing with the exception of the supervision host. If no supervision host certificate has been configured, all pairing is disabled. Host pairing lets the administrator control which devices an iOS 7 device can pair with. <span>Availability: Only in iOS 7.0 and later.</span></p>
          </li>
          <li>
            <p>Disable the 'Today view' in the Notification Center of the lock screen. <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>Prohibit multiplayer gaming.</li>
          <li>
            <p>Allow managed apps and the accounts to only open in other managed apps and accounts. <span>Availability: Only in iOS 7.0 and later.</span></p>
          </li>
          <li>
            <p>Allow unmanaged apps and the accounts will only open in other unmanaged apps and accounts. <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Disable over-the-air PKI updates. Setting this restriction does not disable CRL and OCSP checks.&nbsp; <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Disable Passbook notifications. <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Disable Photo Streams. <span>Availability: Only in iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>Disable the Safari web browser application and remove the icon from the Home screen. This also prevents users from opening web clips.&nbsp;</li>
          <li>Disable Safari auto-fill.</li>
          <li>Enable the Safari fraud warning.</li>
          <li>Prevent Safari from executing JavaScript.</li>
          <li>Prevent Safari from creating pop-up tabs.</li>
          <li>Restrict users from saving a screenshot of the display.</li>
          <li>
            <p>Disable shared Photo Stream. <span>Availability: iOS 6.0 and later.&nbsp;</span></p>
          </li>
          <li>Disable video conferencing.</li>
          <li>Disable voice dialing.</li>
          <li>
            <p>Disable the YouTube application and remove its icon from the home screen. Users will not be able to preview, purchase, or download content too. <span>Availability: iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>Force the use of the profanity filter assistant.</li>
          <li>Encrypt all backups.</li>
          <li>
            <p>Force user to enter their iTunes password for each transaction. <span>Availability: iOS 5.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Limit ad tracking. <span>Availability: iOS 7.0 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Force all devices receiving AirPlay requests from the user's device to use a pairing password. <span>Availability: iOS 7.1 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Force all devices sending AirPlay requests to the user's device to use a pairing password.</p>
          </li>
          <li>Prevent the managed applications from using cloud sync.</li>
          <li>Disable Activity Continuation.</li>
          <li>Prevents the backing up of enterprise books.</li>
          <li>Prevents the syncing of notes and highlights in the enterprise books.</li>
          <li>Allow the user to modify the touch ID.</li>
          <li>Determine the conditions under which the device will accept cookies. The conditions are as follows:<ul>
              <li>Never</li>
              <li>From visited sites only&nbsp;</li>
              <li>Always&nbsp;</li>
            </ul>
          </li>
          <li>
            <p>Force users to unlock their Apple Watch with a passcode once the watch has been removed from their wrist. <span>Availability: iOS 8.3 and later.&nbsp;</span></p>
          </li>
          <li>
            <p>Restrict access to apps based on the rating given for age. The ratings given are as follows:</p>
            <ul>
              <li>Don't allow apps</li>
              <li>4+</li>
              <li>9+</li>
              <li>12+</li>
              <li>17+</li>
              <li>Allow all apps</li>
            </ul>
          </li>
          <li>
            <p>Restrict access to movies based on movie ratings. The ratings given are as follows:</p>
            <ul>
              <li>Don't allow movies</li>
              <li>G</li>
              <li>PG</li>
              <li>PG-13</li>
              <li>R</li>
              <li>NC-17</li>
              <li>Allow all movies</li>
            </ul>
          </li>
          <li>Rate operations based on the region.</li>
          <li>
            <p>Restrict access to TV shows based on the ratings given. <span>The ratings given are as follows:</span></p>
            <ul>
              <li>Don't allow TV shows</li>
              <li>TV-Y</li>
              <li>TV-Y7</li>
              <li>TV-G</li>
              <li>TV-PG</li>
              <li>TV-14</li>
              <li>TV-MA</li>
              <li>All all TV shows</li>
            </ul>
          </li>
          <li>
            <p>Allow the apps to be identified by the bundle IDs listed in the array to autonomously enter Single App Mode. <span>Availability: iOS 7.0 and later.&nbsp;</span></p>
          </li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>
        <p class="p1">Wifi</p>
      </th>
      <td>Configure the Wi-Fi access on a device.</td>
    </tr>
    <tr>
      <th>
        <p class="p1">Email</p>
      </th>
      <td>Configure settings for connecting to your POP or IMAP email accounts.</td>
    </tr>
    <tr>
      <th>AirPlay</th>
      <td>Configure settings for connecting to AirPlay destinations.</td>
    </tr>
    <tr>
      <th>LDAP</th>
      <td>Configure settings for connecting to LDAP servers.</td>
    </tr>
    <tr>
      <th>Calendar</th>
      <td>Configure settings for connecting to CalDAV servers.</td>
    </tr>
    <tr>
      <th>Calendar Subscription</th>
      <td>Configure settings for calendar subscriptions.</td>
    </tr>
    <tr>
      <th>APN</th>
      <td>Specify Access Point Names ( APN ).</td>
    </tr>
    <tr>
      <th>Cellular Network</th>
      <td>Specify Cellular Network Settings on an iOS device</td>
    </tr>
    <tr>
      <th>VPN</th>
      <td>Specify the VPN and per app VPN settings.</td>
    </tr>
  </tbody>
</table>

<!-- </div> -->

<!-- <div class="panelContent" style="background-color: #ffffff;"> -->

### Policies for Windows devices

The mobile device management administrator is able to restrict operations on Windows devices by [adding a new policy](about:blank#). The following policies are available for the Windows platform.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Policies</th>
      <th>Description</th>
    </tr>
    <tr>
      <th>
        <p class="p1">Passcode policy</p>
      </th>
      <td>Define a password policy for the devices.</td>
    </tr>
    <tr>
      <th>Restrictions</th>
      <td>Restrictsthe usage of the camera and other functions. Windows only support device restrictions on the camera.</td>
    </tr>
    <tr>
      <th>Encrypt storage</th>
      <td>Encrypt data on the device, when the device is locked and make it readable when the passcode is entered.</td>
    </tr>
  </tbody>
</table>

<!-- </div> -->
