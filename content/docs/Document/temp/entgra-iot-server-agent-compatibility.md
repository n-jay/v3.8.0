---
bookCollapseSection: true
weight: 4
---

# Entgra IoT Server and Agent Compatibility

This section provides information on the API and device agent versions that are compatible with each release of the [Entgra IoT Server](http://entgra.io/). 

<table>
  <colgroup>
    <col>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>IoT Server version</th>
      <th>API version</th>
      <th>Android agent version</th>
      <th>iOS agent version</th>
    </tr>
    <tr>
      <td>3.0.0</td>
      <td>1.0.0</td>
      <td><a href="https://gitlab.com/entgra/android-agent/tree/v3.4.0" class="external-link" rel="nofollow">3.4.0</a></td>
      <td><a href="https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.0" class="external-link" rel="nofollow">2.0.0</a></td>
    </tr>
    <tr>
      <td><br></td>
      <td><br></td>
      <td><br></td>
      <td><br></td>
    </tr>
  </tbody>
</table>
