---
bookCollapseSection: true
weight: 5
---

# Supported Operations for Mobile Devices

[Entgra IoT Server](http://entgra.io/) currently supports iOS, Android, and Windows mobile devices. However, the device configuration features will vary based on the mobile OS. The device configuration features that are available as listed below.

TODO: ADDRESS ISSUE WITH CONFLUENCE MACROS

<!-- <div class="confluence-information-macro confluence-information-macro-information">

<div class="confluence-information-macro-body"> -->

For more information on the available mobile device management policies, see [Available Mobile Device Management Policies](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/temp/available-mobile-device-management-policies/).
