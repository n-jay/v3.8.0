---
bookCollapseSection: true
weight: 2
---
# Getting the Scope details of an API


To generate an access token that has the necessary permissions to invoke an API, you need to define the required scopes. You can generate the access token by defining [all the device management scopes](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/tutorials/getting-started-with-apis/device-management-api-scopes/) or you can generate the access token that only has the permission or scope to invoke the API you want.  

**In this tutorial**, you get the scope details to invoke a specific API.

Let's get started!

1.  Navigate to the [Entgra API Cloud Store](https://api.cloud.wso2.com/store/?tenant=carbon.super).
2.  Click on the tag you want. Tags are used to group the APIs that fall under similar categories. The APIs that are used to manage the devices are grouped under device_management, the APIs used by the Entgra iOS agent is grouped under iOS and the APIs used by the Android agent are grouped under android.  
    For example, click the** device_management** tag.  
    ![image](352819342.png)
3.  Click on the API segment you want to invoke.   
    For example, click **Android Device Management**.
4.  Click the **API Console** tab.  
    ![image](352819347.png)
5.  Click on the method you want to invoke to see the scope details. Define this scope when obtaining your access token.

    

    

    **NOTE**: If you are generating the access token by using a scope that is specific for an API, you can only invoke that API using the access token.  
    For more information on generating the access token, see [Obtaining the access token](Getting-Started-with-APIs_352819318.html#GettingStartedwithAPIs-Obtainingtheaccesstoken).

    

    

    For example, click the GET device status, method.  
    ![image](352819337.png)

