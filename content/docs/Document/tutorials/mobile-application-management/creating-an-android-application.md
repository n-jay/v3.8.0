---
bookCollapseSection: true
weight: 3
---
# Creating an Android Application


In this tutorial let's take a look at how to publish an Android application the Entgra IoT application store.

Follow the instructions given below.

1.  Navigate to the App Publisher using the following URL:** `https://<IoT_HOST>:9443/publisher`**
2.  [Enter the username and password, and sign in](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console).


3.  Click **Add** **New Mobile Application** that is under the** Mobile Application** drop down.
4.  Select **Android** for **Platform**.
5.  Select the **Store Type**. The available store types are given below:  

    *   **Enterprise** - Refers to all apps that have been created by the organization.
    *   **Public** - Refers to publicly available apps on the Play Store (e.g., free apps available online). 
6.  Creating a new mobile application:

7.  Click **Next**, and you will get the following screen:  
    ![image](352813557.png)  
    Enter the following details of the app.
    *   **Name** - The full name of the app.
    *   **Display Name** - The name of the app that is displayed to the user.
    *   **Description** -  A summarized description of the app.
    *   **Recent Changes** - Optional. A summarized description of what is new in this app when compared to its previous version.
    *   **Version - **Version of the public application. If you have previously entered a different version of this app, make sure to enter a unique version number.
    *   **Category** - Select the category that this app needs to be listed under.
    *   **Visibility** - Enter the user roles that this app will be visible to.
    *   **Tags** - You can group mobile applications under a specific category using tags.  Define the category and press enter.
    *   Click on the respective **+** buttons to upload the required images.
    *   *   **Banner** - Image that will appear as the app banner.
        *   **Screenshots** - Screenshots of the app so that the user can get an understanding of what the app offers. Amaximumoffourscreenshotsare allowed.
        *   **Icon file** - The image that will be used as the application icon in the Store and when the application is installed on a device.

    

    

    The recommended image extension is `.png` while the recommended dimensions are as follows:

    *   **Icons**: 124px x 124px
    *   **Screenshots**: 288px x 512px (landscape) and 512px x 288px (portrait)
    *   **Banners**: 705px x 344px

    You must follow the above standards when creating a webclip. A web clip is used to add a bookmark to a web application. 

    

    

8.  Click **Create**. The created app will appear in the created list of apps.





After creating the mobile app, authorized users can review, approve, and publish the mobile application. For more information, see [Mobile Application Lifecycle Management](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Managing-Mobile-Applications/#mobile-application-lifecycle-management).



