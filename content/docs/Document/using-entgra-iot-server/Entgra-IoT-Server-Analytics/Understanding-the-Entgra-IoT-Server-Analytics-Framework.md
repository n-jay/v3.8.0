# Understanding the Entgra IoT Server Analytics Framework

Before moving on to write analytics for your device, let's get an understanding of the basic architecture behind Entgra IoT Server's analytics.

The Entgra IoT Analytics Framework combines real-time, batch, interactive, and predictive (via machine learning) analysis of data into one integrated platform to support the multiple demands of the Internet of Things (IoT) solutions, as well as mobile and Web apps as illustrated by the image shown below:

![image](352822463.png)

As a part of the Entgra IoT Server analytics platform, WSO2 Data Analytics Server (WSO2 DAS) and WSO2 Complex Event Processor (WSO2 CEP) introduces a single solution with the ability to build systems and applications that collect and analyze both real-time and historical data and communicate the results. It is designed to treat millions of events per second and is capable of handling Big Data volumes and Internet of Things projects.

WSO2 DAS/CEP not only process data as events but also interact with external systems using events. An event is a unit of data, and an event stream is a sequence of events of a particular type. Entgra IoT Server Analytics Framework compromises of aggregating, analyzing and presenting a given IoT related use case in a proper way. This definition is paramount when designing a solution to address a data analysis use case. Aggregation refers to the collection of data, analysis refers to the manipulation of data to extract information, and presentation refers to representing this data visually or in other ways such as alerts. Data which you need to be monitor or process sequentially go through these modules. 





Before going forward, get yourself familiar with these buzzwords (**Event receivers, Event streams, Event processors, Event publishers, Data Store, Analytics Spark, Data Indexing, Analytics Dashboard, Event Sinks**). For more information, see the [WSO2 DAS architecture](https://docs.wso2.com/display/DAS310/Architecture).

## Analyzing Data

Data can be processed as batch or real-time or combination of both.

Events can be processed in real-time using WSO2's Siddhi Complex Event Processor (CEP) engine. Before we begin let's understand  the basic components which are used it real time processing. A stream is defined as a sequence of events having the same data type. It can be processed using the Siddhi Query Language (SiddhiQL). Siddhi works as an upside down database by storing all the queries in memory, matching incoming events against the stored queries at runtime and producing events that matched as streams. In order to process events, the execution logic should be written as a script using SiddhiQL and deployed in the server. This execution plan will act as an isolated execution environment and there will be an instance of the Siddhi Execution Runtime created for each execution plan. An execution plan can import one or more streams from the server for processing and push zero or more output streams back to the server.  To import and export streams, the script needs to contain import and export annotations that binding the streams defined in the execution plan to the server.

On the other hand, in order to perform batch analytics data needs to be  retrieved from the data stores and various analysis operations needs to be performed according to the defined analytic queries.

The following sections describe how to work with these components to analyze your stored data:

*   **[Interactive Analytics](https://docs.wso2.com/display/DAS310/Interactive+Analytics)**

    

    

    Follow the WSO2 DAS documentation on how you can perform interactive analytics using the Apache Lucene Query Language.

    

    

*   **[Batch Analytics Using Spark SQL](https://docs.wso2.com/display/DAS310/Batch+Analytics+Using+Spark+SQL)**

    

    

    Apache Spark is a powerful open-source processing engine built around speed, ease of use, and sophisticated analytics. WSO2 DAS employs [Apache Spark](http://spark.apache.org/) as its analytics engine. Follow the WSO2 DAS documentation on how you can perform batch analytics using Apache Spark SQL.[  ](https://docs.wso2.com/display/DAS310/Batch+Analytics+Using+Spark+SQL)

    

    

*   **[Real-time Analytics Using Siddhi](https://docs.wso2.com/display/DAS310/Realtime+Analytics+Using+Siddhi)**

    

    

    Follow the WSO2 DAS documentation on how you can perform real-time analytics using Siddhi in WSO2 DAS.

    

    

*   **[Predictive Analytics Using WSO2 ML](https://docs.wso2.com/display/DAS310/Predicitve+Analytics)**

    

    

    Deploy datasets and generates predictive models via the Machine Learner Wizard. Follow WSO2 DAS and WSO2 Machine Learner (ML) documentation for more information on this section.

    

    
## Collecting data

The first step is to collect the data that needs to be processed, which are stored in a data store, where it is optimized for analysis.  The following sections describe how to work with these components to aggregate your data:

### Publishing data

The following subsections will guide you on how to publish data to WSO2 Data Analytics Server (DAS) or WSO2 Complex Event Processor (CEP). 





You will be directed to the WSO2 DAS/CEP documentation.





*   **[Publishing Data Using Event Simulation](https://docs.wso2.com/display/CEP410/Publishing+Data+Using+Event+Simulation)**

    

    

    The event simulator tool is used to simulate predefined event streams. These event stream definitions have stream attributes. You can use event simulator to create events by assigning values to the defined stream attributes and send them as events. This tool is useful for debugging and monitoring the event receivers and publishers, execution plans and event formatters. The events are sent to the component (e.g. to an event receiver, event publisher, execution plan etc.) that is defined in the event stream. For more information check out this section in the WSO2 CEP documentation.

    

    

*   **[Publishing Data using Binary/Thrift Client (Java)](https://docs.wso2.com/pages/viewpage.action?pageId=49777972)**

    

    

    A data publisher allows you to send data to a predefined set of data fields in a DAS/CEP server. The data structure with predefined fields is defined in an event stream. The data is converted to the format defined by the event stream and sent via the WSO2 data-bridge component. You can also send custom key-value pairs with data events.

    

    

*   **[Publishing Data Through Other Protocols](https://docs.wso2.com/display/CEP410/Publishing+Data+Through+Other+Protocols)**

    

    

    Data agents are used to collecting large amounts of data about services, mediators etc. from various data collection points such as ESB, application servers, and custom data publishers, and pump them to data analysis and summarization servers such as WSO2 DAS and WSO2 Complex Event Processor. For more information on this, check out the WSO2 CEP documentation.

    

    

### Persisting data

For more information, see the WSO2 DAS documentation on [persisting Data for Batch Analytics](https://docs.wso2.com/display/DAS310/Persisting+Data+for+Batch+Analytics).

### Configuring data

The following sections cover the different types of receivers  which come out of the box in WSO2 DAS/CEP. In addition, how to define your own custom receiver, which gives more flexibility to receive events that are sent to CEP/DAS. 


You will be directed to the WSO2 CEP documentation.

*   [Input Mapping Types](https://docs.wso2.com/display/CEP410/Input+Mapping+Types)

### Configuring Event Receivers

#### Event receiver types

WSO2 CEP/DAS has the capability of receiving events from event receivers via various transport protocols. Following are the event receivers that come with WSO2 CEP/DAS by default. You can write extensions to support other transport.

*   Email Event Receiver
*   File-tail Event Receiver
*   HTTP Event Receiver
*   OAuth enabled HTTP Event Receiver
*   JMS Event Receiver
*   Kafka Event Receiver
*   MQTT Event Receiver
*   OAuth enabled MQTT Event Receiver
*   XMPP Event Receiver
*   SOAP Event Receiver
*   WebSocket Event Receiver
*   WebSocket Local Event Receiver
*   WSO2Event Event Receiver

#### Event receiver configuration

An event receiver configuration has four main sections as shown in the example below. 

![image](352822507.png)

Event receiver configurations are stored in the file system as hot deployable artifacts in the <`PRODUCT` `_HOME>/repository/deployment/server/eventreceivers/ ` directory as shown in the example below. 


<eventReceiver name="WSO2EventEventReceiver" statistics="disable"
    trace="disable" xmlns="http://wso2.org/carbon/eventreceiver">
    <from eventAdapterType="wso2event">
        <property name="events.duplicated.in.cluster">false</property>
    </from>
    <mapping customMapping="disable" type="wso2event"/>
    <to streamName="testEventStream" version="1.0.0"/>
</eventReceiver>

The above sections of an event receiver configuration are described below.

<table>
  <tbody>
    <tr>
      <th>
        
      </th>
      <th>
        
      </th>
    </tr>
    <tr>
      <td><strong>From</strong></td>
      <td><span>An input event adapter (transport) configuration via which the event receiver receives events.</span></td>
    </tr>
    <tr>
      <td><strong>Adapter properties</strong></td>
      <td>Specific properties of the selected input event adapter. For information on configuring adapter properties of various transport types, see <a href="#ConfiguringEventReceivers-EventReceiverTypes">Event Receiver Types</a>.</td>
    </tr>
    <tr>
      <td><strong>To</strong></td>
      <td><span>The event stream from which the event receiver will fetch the events for processing.</span></td>
    </tr>
    <tr>
      <td><strong>Mapping configuration</strong></td>
      <td><span>The format of the message that is received. You can configure custom mappings on the selected format via advanced settings. <span>For information on configuring custom mappings, see Input Mapping Types.</span></span></td>
    </tr>
  </tbody>
</table>

#### Creating event receivers

You can create event receivers either [using the management console](about:blank#ConfiguringEventReceivers-Creatingreceiversusingthemanagementconsole) or [using a configuration file](about:blank#ConfiguringEventReceivers-Creatingreceiversusingaconfigurationfile) as explained below.

##### Creating receivers using the management console 

Follow the steps below to create an event receiver using the management console of WOS2 CEP/DAS.





To create an event receiver via the management console, you must have at least one event stream defined.





1.  Log in to the management console, and click **Main.**
2.  Click** Receivers** in the **Event** menu, and then click** Add Event Receiver**.
3.  Enter a name for **Event Receiver Name**. (Do not use spaces between the words in the name of the event receiver.)

4.  Select the input transport from which you want to receive events for the  **Input Event Adapter Type**, and enter the **Adapter Properties** accordingly. For instructions on the adapter properties of input transport types, see [Event Receiver Types](about:blank#ConfiguringEventReceivers-EventReceiverTypes) [.](https://docs.wso2.com/display/CEP400/Event+Receiver+Types)

5.  Select the **Event Stream**, to which you want to map the received events.

6.  Select the **Message Format** which you want to apply on the receiving events. WSO2 servers allow users to configure events in XML, JSON, Text, Map, and WSO2Event event formats.

7.  Click **Advanced** to define custom input mappings based on the message format you selected, if you are sending events that do not adhere to the default event formats. For more information on custom input mapping types, see Input Mapping Types.
8.  Click **Add Event Receiver**,to create the event receiver in the system. When you click  **OK**  in the pop-up message on successful addition of the event receiver, you view it in the  **Available Event Receivers**  list as shown below.  
    ![image](352822502.png)

#### Creating receivers using a configuration file

Follow the steps below to create an event receiver using a configuration file.

1.  Create an XML file with the following event receiver configurations. An event receiver implementation must start with  `<eventReceiver`>``  as the root element.

    

    

    In the following configuration, specify the respective adapter properties based on the transport type of the receiver within the `<from>` element. For the respective adapter properties of the event receiver configuration based on the transport type, see [Event Receiver Types](about:blank#ConfiguringEventReceivers-EventReceiverTypes) .

    

    

    <eventReceiver name="EVENT-RECEIVER-NAME" statistics="disable" trace="disable" xmlns="http://wso2.org/carbon/eventreceiver">
        <from eventAdapterType="EVENT-ADAPTER-TYPE">
           .................................
        </from>
        <mapping customMapping="disable" type="xml"/>
        <to streamName="Test Stream" version="1.0.0"/>
    </eventReceiver>

    The properties of the above configuration are described below.

    <table>
      <thead>
        <tr>
          <th>Adapter property</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            
          </td>
          <td>Name of the event receiver</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Whether monitoring event statistics is enabled for the receiver</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Whether tracing events is enabled for the receiver</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>XML namespace for event receivers</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Type of the event adapter.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Whether a custom mapping is enabled on the receiver.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Type of the enabled custom mapping.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Name of the event stream to which the receiver is mapped.</td>
        </tr>
      </tbody>
    </table>

2.  Add the XML file to the `<PRODUCT_HOME>/repository/deployment/server/eventreceivers/` directory. Since hot deployment is supported in the product, you can simply add/remove event receiver configuration files to deploy/undeploy event receivers to/from the server.





First define the stream to which the receiver is publishing data to activate the receiver. When receiving WSO2Events, the incoming stream definition that you select in the advanced input mappings must also be defined, to activate the event receiver. When you click **Inactive Event Receivers** in the **Available Event Receivers** screen, if an event receiver is in the inactive state due to some issue in the configurations, you view a short message specifying the reason why the event receiver is inactive as shown below. A similar message is also printed on the CLI.





![image](352822512.png)

After a receiver is successfully added, it gets added to the list of receivers displayed under  **Event**   in the  **Main**   menu of the product's management console. Click **Edit **   to change its configuration and redeploy it. This opens an XML-based editor allowing you to edit the event receiver configurations from the UI. Do your modifications and click  **Update** . You can also delete it, enable/disable statistics or enable/disable tracing on it using the provided options in the UI as described below.

#### Enabling statistics for event receivers

Follow the steps below to enable monitoring statistics of events received by an existing event receiver.

1.  Log in to the management console, and click **Main.**
2.  Click** Receivers** in the **Event** menu. You view the **Available Event Receivers** list.
3.  Click the **Enable Statistics** button of the corresponding event receiver to enable monitoring event statistics for it.

#### Enabling tracing for event receivers

Follow the steps below to enable tracing on events received by an existing event receiver.

1.  Log in to the management console, and click **Main.**
2.  Click** Receivers** in the **Event** menu. You view the **Available Event Receivers** list.
3.  Click the **Enable Tracing** button of the corresponding event receiver to enable event tracing for it.

#### Deleting event receivers

Follow the steps below to delete an existing event receiver.

1.  Log in to the management console, and click **Main.**
2.  Click** Receivers** in the **Event** menu. You view the **Available Event Receivers** list.
3.  Click the  **Delete** button of the corresponding event receiver to delete it.

#### Editing event receivers

Follow the steps below to edit an existing event receiver.

1.  Log in to the management console, and click **Main.**
2.  Click** Receivers** in the **Event** menu. You view the **Available Event Receivers** list.
3.  Click the  **Edit** button of the corresponding event receiver to edit it. This opens **Edit Event Receiver Configurations** XML editor.  
4.  After editing, click **Update**, to save the configuration, or click **Reset** to reset the configuration to its original state.

### Building Custom Event Receivers

Events are received by WSO2 CEP/DAS server using event receivers, which manage the event retrieval process. Event receiver configurations are stored in the file system as deployable artifacts. WSO2 CEP/DAS receives events via multiple transports in JSON, XML, Map, Text, and WSO2Event formats, and converts them into streams of canonical WSO2Events to be processed by the server. 

In addition to the default receiver types, you can define your own custom receiver, which gives more flexibility to receive events that are sent to Entgra products. Since each event receiver implementation is an OSGI bundle, you can deploy/undeploy it easily on the Entgra product. To create a custom event receiver, import **org.wso2.carbon.event.input.adaptor.core** package with the provided skeleton classes/interfaces required by a custom receiver implementation.

#### Implementing InputEventAdapter Interface

**org.wso2.carbon.event.input.adapter.core.InputEventAdapter** interface contains the event receiver logic that will be used to receive events. You should override the below methods when implementing your own custom receiver.

1.  `void init(InputEventAdapterListener eventAdaptorListener) throws` `InputEventAdapterException` This method is called when initiating event receiver bundle. Relevant code segments which are needed when loading OSGI bundle can be included in this method.

2.  `void testConnect() throws TestConnectionNotSupportedException, InputEventAdapterRuntimeException, ConnectionUnavailableException`

    This method checks whether the receiving server is available.

3.  `void connect() throws InputEventAdapterRuntimeException, ConnectionUnavailableException`

    The method connect`()` will be called after calling the `init()` method. The intention is to connect to a receiving end and if it is not available the `ConnectionUnavailableException` will be thrown.

4.  `void disconnect()`

     The `disconnect()` method can be called when it is needed to disconnect from the connected receiving server.

5.  `void destroy()`

    The method can be called when removing an event receiver. The cleanups have to be done when removing the receiver can be done over here.

6.  `boolean isEventDuplicatedInCluster()`

    Returns a boolean output stating whether an event is duplicated in a cluster or not. This can be used in clustered deployment.

7.  `boolean isPolling()`

    Checks whether events get accumulated at the adapter and whether the clients connect to it to collect events. 

Below is a sample File Tail Receiver implementation of the methods described above: 

public class FileTailEventAdapter implements InputEventAdapter {

    @Override
    public void init(InputEventAdapterListener eventAdapterListener) throws InputEventAdapterException {
        validateInputEventAdapterConfigurations();
        this.eventAdapterListener = eventAdapterListener;
    }

    @Override
    public void testConnect() throws TestConnectionNotSupportedException {
        throw new TestConnectionNotSupportedException("not-supported");
    }

    @Override
    public void connect() {
        createFileAdapterListener();
    }

    @Override
    public void disconnect() {
        if (fileTailerManager != null) {
            fileTailerManager.getTailer().stop();
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public boolean isEventDuplicatedInCluster() {
        return Boolean.parseBoolean(globalProperties.get(EventAdapterConstants.EVENTS_DUPLICATED_IN_CLUSTER));
    }

    @Override
    public boolean isPolling() {
        return true;
    }

    private void validateInputEventAdapterConfigurations() throws InputEventAdapterException {
        String delayInMillisProperty = eventAdapterConfiguration.getProperties().get(FileTailEventAdapterConstants.EVENT_ADAPTER_DELAY_MILLIS);
        try{
            Integer.parseInt(delayInMillisProperty);
        } catch (NumberFormatException e){
            throw new InputEventAdapterException("Invalid value set for property Delay: " + delayInMillisProperty, e);
        }
    }

    private void createFileAdapterListener() {
        if(log.isDebugEnabled()){
            log.debug("New subscriber added for " + eventAdapterConfiguration.getName());
        }
        String delayInMillisProperty = eventAdapterConfiguration.getProperties().get(FileTailEventAdapterConstants.EVENT_ADAPTER_DELAY_MILLIS);
        int delayInMillis = FileTailEventAdapterConstants.DEFAULT_DELAY_MILLIS;
        if (delayInMillisProperty != null && (!delayInMillisProperty.trim().isEmpty())) {
            delayInMillis = Integer.parseInt(delayInMillisProperty);
        }
        boolean startFromEnd = false;
        String startFromEndProperty = eventAdapterConfiguration.getProperties().get(FileTailEventAdapterConstants.EVENT_ADAPTER_START_FROM_END);
        if (startFromEndProperty != null && (!startFromEndProperty.trim().isEmpty())) {
            startFromEnd = Boolean.parseBoolean(startFromEndProperty);
        }
        String filePath = eventAdapterConfiguration.getProperties().get(
                FileTailEventAdapterConstants.EVENT_ADAPTER_CONF_FILEPATH);
        FileTailerListener listener = new FileTailerListener(new File(filePath).getName(), eventAdapterListener);
        Tailer tailer = new Tailer(new File(filePath), listener, delayInMillis, startFromEnd);
        fileTailerManager = new FileTailerManager(tailer, listener);
        singleThreadedExecutor.execute(tailer);
    }
}

#### Implementing InputEventAdapterFactory Class

**org.wso2.carbon.event.input.adapter.core. InputEventAdapterFactory** class can be used as the factory to create your appropriate event receiver type. You should override the below methods when extending your own custom receiver.

1.  `public String getType()`

    This method returns the receiver type as a String.

2.  `public List<String> getSupportedMessageFormats()`

    Specify supported message formats for the created receiver type.

3.  `public List<Property> getPropertyList()`

    Here the properties have to be defined for the receiver. When defining properties you can implement to configure property values from the management console.

4.  `public String getUsageTips()`

    Specify any hints to be displayed in the management console.

5.  `public InputEventAdapter createEventAdapter(InputEventAdapterConfiguration eventAdapterConfiguration, Map<String, String> globalProperties)`

    This method creates the receiver by specifying event adapter configuration and global properties which are common to every adapter type.

Below is a sample File Tail Receiver implementation of the I`nputEventAdapterFactory` class: 

public class FileTailEventAdapterFactory extends InputEventAdapterFactory {
    @Override
    public String getType() {
        return FileTailEventAdapterConstants.EVENT_ADAPTER_TYPE_FILE;
    }

    @Override
    public List<String> getSupportedMessageFormats() {
        List<String> supportInputMessageTypes = new ArrayList<String>();
        supportInputMessageTypes.add(MessageType.TEXT);
        return supportInputMessageTypes;
    }

    @Override
    public List<Property> getPropertyList() {
        List<Property> propertyList = new ArrayList<Property>();
        Property filePath = new Property(FileTailEventAdapterConstants.EVENT_ADAPTER_CONF_FILEPATH);
        filePath.setDisplayName(
                resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_CONF_FILEPATH));
        filePath.setRequired(true);
        filePath.setHint(resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_CONF_FILEPATH_HINT));
        propertyList.add(filePath);

        Property delayInMillis = new Property(FileTailEventAdapterConstants.EVENT_ADAPTER_DELAY_MILLIS);
        delayInMillis.setDisplayName(
                resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_DELAY_MILLIS));
        delayInMillis.setHint(resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_DELAY_MILLIS_HINT));
        propertyList.add(delayInMillis);
        Property startFromEndProperty = new Property(FileTailEventAdapterConstants.EVENT_ADAPTER_START_FROM_END);
        startFromEndProperty.setRequired(true);
        startFromEndProperty.setDisplayName(
                resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_START_FROM_END));
        startFromEndProperty.setOptions(new String[]{"true", "false"});
        startFromEndProperty.setDefaultValue("true");
        startFromEndProperty.setHint(resourceBundle.getString(
                FileTailEventAdapterConstants.EVENT_ADAPTER_START_FROM_END_HINT));
        propertyList.add(startFromEndProperty);
        return propertyList;
    }

    @Override
    public String getUsageTips() {
        return resourceBundle.getString(FileTailEventAdapterConstants.EVENT_ADAPTER_USAGE_TIPS_FILE);
    }

    @Override
    public InputEventAdapter createEventAdapter(InputEventAdapterConfiguration eventAdapterConfiguration,
                                                Map<String, String> globalProperties) {
        return new FileTailEventAdapter(eventAdapterConfiguration, globalProperties);
    }

}

#### Exposing Custom Event Receiver as an OSGI Service

Apart from above, you can maintain a service class under **internal\ds\** directory to expose the custom event receiver implementation as an OSGI service. When exposing the service, it needs to expose as “InputEventAdaptorFactory” type. Below is a sample implementation for a service class for a File Tail Receiver:

/**
 * @scr.component component.name="input.File.AdapterService.component" immediate="true"
 * @scr.reference name="configurationcontext.service"
 * interface="org.wso2.carbon.utils.ConfigurationContextService" cardinality="1..1"
 * policy="dynamic" bind="setConfigurationContextService" unbind="unsetConfigurationContextService"
 */

public class FileTailEventAdapterServiceDS {
    private static final Log log = LogFactory.getLog(FileTailEventAdapterServiceDS.class);

    protected void activate(ComponentContext context) {
        try {
            InputEventAdapterFactory testInEventAdapterFactory = new FileTailEventAdapterFactory();
            context.getBundleContext().registerService(InputEventAdapterFactory.class.getName(),
                    testInEventAdapterFactory, null);
            if (log.isDebugEnabled()) {
                log.debug("Successfully deployed the TailFile input event adapter service");
            }
        } catch (RuntimeException e) {
            log.error("Can not create the TailFile input event adapter service ", e);
        }
    }

    protected void setConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        FileTailEventAdapterServiceHolder.registerConfigurationContextService(configurationContextService);
    }

    protected void unsetConfigurationContextService(
            ConfigurationContextService configurationContextService) {
        FileTailEventAdapterServiceHolder.unregisterConfigurationContextService(configurationContextService);
    }
}



Furthermore, you can have a utility directory _as **internel\util\**_ where you can place utility classes required for the custom receiver implementation.





#### Deploying Custom Event Receiver

Deploying a custom event receiver is very simple in WSO2 CEP 4.0.0\. Simply implement the custom event receiver type, build the project and copy the created OSGI bundle that is in the "target" folder into the `**<CEP_HOME>/repository/components/dropins**`. In CEP server startup, you can see the newly created event receiver type service in the server startup logs. The newly created custom event receiver type will also be visible in the UI with necessary properties. Now you can create several instances of this event receiver type.
    

    

## Communicating Results

Once the data is collected and analyzed to produce meaningful information, the final step is to communicate this information. The information produced is communicated as follows:

### Visualizing Results
   

    Create dashboards using summarized analytics tables and event streams.  A dashboard is a container for gadgets/widgets, and its pages have a structured layout with predefined grids, to which you add the dashboard components. 

    You can create dashboards using summarized analytics tables and event streams.  A dashboard is a container for gadgets/widgets, and its pages have a structured layout with predefined grids, to which you add the dashboard components. 





Note that this dashboard is not supported with IBM JDK.









You can deploy a dashboard and/or its components (e.g. layouts, gadgets, and widgets) in the Analytics Dashboard of WSO2 DAS by bundling them as artifacts of a Carbon Application (C-App). For instructions on deploying C-Apps in WSO2 DAS, see  [Packaging Artifacts as a C-App Archive](https://docs.wso2.com/display/DAS301/Packaging+Artifacts+as+a+C-App+Archive). In Entgra IoTS too we will be packaging the analytics components as a C-APP. For more information, see [Writing Analytics](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-analytics).






Let's take a look at how you can create a dashboard to visualize the data: 

1.  [Sign into the Entgra device management console](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console).

2.  Click the user icon, and click **Dashboard Settings**.  
    Example:  
    ![image](352822580.png)
3.  Click **DASHBOARDS** in the path to navigate to the portal home page.  
    ![image](352822586.png)  
    The WSO2 Dashboard Designer home page appears with the dashboards that are available for the respective user.  
    ![image](352822550.png)
4.  Click **Create Dashboard** to navigate to the dashboard creation page.  
    ![image](352822555.png)
5.  Enter the dashboard related details and click **Next**.  

    *   **Name of your Dashboard** - The name given to identify the dashboard.
    *   **URL** - The URL that is used to access the dashboard. The URL should be unique for each dashboard.
    *   **Description** - A summarized description on the dashboard.This directs you to the create dashboard page, where you can create a page for the dashboard and select a predefined layout for the page.
6.  If you wish to create a dashboard with a banner, follow the instructions below and skip step 5.

    

    

    For more information, see the WSO2 Dashboard Server (DS) documentation on [Adding a Banner to a Dashboard](https://docs.wso2.com/display/DS200/Adding+a+Banner+to+a+Dashboard).

    

    

7.  If you wish to create a dashboard without a banner, follow the steps below:

    1.  Click on a preferred page layout.   
        As shown in the above screenshot, WSO2 DS provides six layouts to choose from.  
        ![image](352822560.png)This directs you to the dashboard edit page, which provides you a 
        host of options to add gadgets to the dashboard being created.  
        ![image](352822575.png)

8.  Customize the dashboard as required. 

    

    

    For more information, see the WSO2 DS documentation on [Editing a Dashboard](https://docs.wso2.com/display/DS200/Editing+a+Dashboard).

    

    

9.  Optionally, add a page.

    

    

    For more information, see the WSO2 DS documentation on [Adding a Page in a Dashboard](https://docs.wso2.com/display/DS200/Adding+a+Page+in+a+Dashboard).

    

    

10.  Optionally, enable inter-gadget communication.

    

    

    For more information, see the WSO2 DS documentation on [Enabling Inter-gadget Communication](https://docs.wso2.com/display/DS200/Enabling+Inter-gadget+Communication).

    

    

11.  Optionally, enable dashboard security.

    

    

    If a dashboard is not secured, all users are allowed to access and edit the dashboard.  
    For more information, see the WSO2 DS documentation on [Securing a Dashboard](https://docs.wso2.com/display/DS200/Securing+a+Dashboard). 

    

    

    

    

*   **[Creating Alerts](https://docs.wso2.com/display/DAS310/Creating+Alerts)**

    

    

    Events can be notified or published to external systems from the WSO2 IoT server using event publishers. Event publishers enable you to manage event publishing and notifications. They allow publishing events via multiple transports in JSON, XML, Map, text, and WSO2Event formats to various endpoints and data stores.

    

    

*   [**Communicating Results Through REST AP**I](https://docs.wso2.com/display/DAS310/REST+APIs+for+Analytics+Data+Service)

    

    

    WSO2 DAS stores the received events and processed data in its underlying data storage system, where that data can be retrieved using the standards APIs that have been defined. Analytics REST API allows external  
    parties to query this data. When this API is used independently of the backend data store, it can be used to lookup/search for data that is stored in the system.[ ](https://docs.wso2.com/display/DAS310/Communicating+Results+Through+REST+API)

    

    

*   **[Analytics JavaScript (JS) API](https://docs.wso2.com/display/DAS310/Analytics+JavaScript+%28JS%29+API)**

    

    

    Analytics JavaScript API exposes WSO2 DAS analytics functionalities as JavaScript functions. You can use this JavaScript API in your Web apps to perform analytics operations when creating Web apps or Dashboard gadgets.





