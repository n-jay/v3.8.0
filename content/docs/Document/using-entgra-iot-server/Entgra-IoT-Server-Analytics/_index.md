---
bookCollapseSection: true
weight: 5
---

# Entgra IoT Server Analytics




The following sections describe how you can use Entgra IoT Server analytics.

*   **[Understanding the Entgra IoT Server Analytics Framework](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Entgra-IoT-Server-Analytics/Understanding-the-Entgra-IoT-Server-Analytics-Framework/)**
*   **[Monitoring Devices Using Location Based Services](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Entgra-IoT-Server-Analytics/Monitoring-Devices-Using-Location-Based-Services/)**
*   **[Publishing Operation response to Analytics](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Entgra-IoT-Server-Analytics/Publishing-Operation-response-to-Analytics/)**
