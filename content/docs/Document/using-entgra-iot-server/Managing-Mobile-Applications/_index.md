---
bookCollapseSection: true
weight: 4
---
# Managing Mobile Applications




## Application Management REST APIs

Entgra IoT Server is 100% API driven. Therefore, you can create, publish and install the application using the application management APIs. 

### Obtain the access token

You can obtain an access token by providing the resource owner's username and password as an authorization grant. It requires the base64 encoded string of the `consumer-key:consumer-secret` combination. Let's take a look at how it's done.

1.  Encode the client credentials as follows:

2.  Generate the Client ID and the Secret ID.

3.  Encode the client credentials as follows:

4.  Generate the access token using the following command:

    

    

    The access token you generated expires in an hour. After it expires you can generate a new access token using the refresh token as shown below.

    

    

### App management APIs

*   **[App publisher APIs](https://docs.wso2.com/display/IOTS/iot-server/publisher)**
*   **[App store APIs](https://docs.wso2.com/display/IOTS/iot-server/store)**

## Enabling Enterprise Subscriptions for Mobile Apps

Want to install an application based on the employee's role in the Organization or install an application on specific employees devices? You can enterprise install applications on many devices via the Entgra IoT Server's app store.

Example: Your Inventory officers keeping track of inventory require a barcode reader to scan the products in order to keep track of the inventory coming in and going out of the organization. If you do have a specific mobile app for this purposes, you can use this option to install the barcode reader mobile application on all the inventory officers, mobile devices. 

Follow the steps below to carry-out enterprise subscriptions for mobile apps:

1.  Set the value of the `EnterpriseOperations_Enabled` configuration in the `<IOTS_HOME>/conf/app-manager.xml` file to `true` as follows.

    `<Config name="EnterpriseOperations_Enabled">true</Config>`

2.  Enter the name of the user role to whom you want to assign privileges to enable enterprise subscriptions, as the value of the `EnterpriseOperations_AuthorizedRole` configuration in the `<IOTS_HOME>/conf/app-manager.xml` file as follows.

    

    

    The super tenant user in IoT Server has access to this feature by default.

    

    

    Example:

    `<Config name="EnterpriseOperations_AuthorizedRole">Internal/store-admin</Config>`

3.  Log in to the Entgra IoT Server store using the following URL as a user with a privileged role as defined in the above step: `https://<IoT_HOST>:9443/store/`

4.  Click on the mobile app on which you want to apply enterprise subscriptions.

5.  Click **Ent. Install** as shown below.

    ![image](352822348.png) 

6.  Select the roles to whom you want to enable enterprise subscription on the app as shown below.

    ![image](352822342.png)

7.  Click **Instal**l.

8.  Select the users for whom you want to enable enterprise subscription on the app as shown below.

    ![image](352822336.png) 

9.  Click **Instal**l.

10.  Click **Yes** in the pop-up message. You view another pop-up message upon successful subscription.

11.  Log in to the App Store as a user on whom enterprise subscription was applied above, using the following URL: `https://<IOTS_HOST>:9443/store/`

12.  Click **My Subscriptions**. You will have the app on which enterprise subscription was applied added to your **My Subscriptions** list.

## Mobile Application Lifecycle Management

You can manage the lifecycle of Web applications you create in Entgra App Manager as described below.

### Introduction to app lifecycle management

The application (app) approval process involves all the states in an application lifecycle from creation to retirement. Your actions are restricted based on the permissions that are assigned to you. The following topics illustrate the application approval process.

#### App lifecycle management action definitions

 You can carry out the following actions in the application approval process.

<table>
  <tbody>
    <tr>
      <th>Action</th>
      <th>Definition</th>
    </tr>
    <tr>
      <td><strong>Submit</strong></td>
      <td>
        <p>The application creator submits the newly created apps to be reviewed.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Approve</strong></td>
      <td>
        <p>The reviewer approves the apps that pass the review process.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Reject</strong></td>
      <td>
        <p>The reviewer rejects mobile apps that fail the review process.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Publish</strong></td>
      <td>
        <p>The publisher publishes the approved apps. All published apps are visible in the App Store.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Unpublish</strong></td>
      <td>
        <p>The publisher un-publishes apps if required. Unpublished apps are temporarily removed from the App Store but are supported by App Manager. Furthermore, you can re-publish these apps to the Store whenever needed.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Depreciate</strong></td>
      <td>
        <p>The publisher deprecates mobile apps. All deprecated apps are automatically removed from the Store, to stop all new installations. However, these mobile apps are supported by App Manager.</p>
      </td>
    </tr>
    <tr>
      <td><strong>Retire</strong></td>
      <td>
        <p>The publisher retires apps. Retired apps are not supported by App Manager in the future. In such instances, all retired apps are automatically removed from the Store, to stop all new installations.</p>
      </td>
    </tr>
  </tbody>
</table>

#### App lifecycle management based on user roles

The transitions in the app approval process, together with the permissions needed for each state are listed as follows:

<table>
  <tbody>
    <tr>
      <th>Transition Process</th>
      <th>Allowed Roles</th>
      <th>Allowed Actions</th>
    </tr>
    <tr>
      <td>Creating a new app</td>
      <td>Administrator<br>Internal/Creator</td>
      <td><strong>Publish</strong></td>
    </tr>
    <tr>
      <td>Submitting newly created apps</td>
      <td>
        <p>Administrator<br>Internal/Creator</p>
      </td>
      <td><strong>Submit</strong></td>
    </tr>
    <tr>
      <td>Reviewing submitted apps</td>
      <td>Administrator</td>
      <td><strong>Approve/</strong><br><strong>Reject&nbsp;</strong></td>
    </tr>
    <tr>
      <td>Publishing approved apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Publish</strong></td>
    </tr>
    <tr>
      <td>Re-submitting rejected apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Submit</strong></td>
    </tr>
    <tr>
      <td>Unpublishing published apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Unpublish</strong></td>
    </tr>
    <tr>
      <td>Re-publishing unpublished apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Publish</strong></td>
    </tr>
    <tr>
      <td>Deprecating unpublished apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Deprecate</strong></td>
    </tr>
    <tr>
      <td>Deprecating published apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Deprecate</strong></td>
    </tr>
    <tr>
      <td>Retiring deprecated apps</td>
      <td>Administrator<br>Internal/Publisher</td>
      <td><strong>Retire</strong></td>
    </tr>
  </tbody>
</table>

### App lifecycle management states

After adding the application to the publisher, you need to promote its lifecycle state to the published state, to publish it. Take a look at the diagram given below to get an understanding of the app management lifecycle.

![image](352822364.png)   
Once an application is added to App Manager, it is added to the registry as an asset, and a lifecycle is attached to the application. The lifecycle state for a newly created application is `Created`. You can change the state to `In-Review,` which will automatically initialize a workflow event and then `Approve` the application or `Reject` the application, which will bring it back to the in-review state. Once an admin user approves the workflow event, then the lifecycle state moves to the `Published` state and will appear in the Store for you to consume.

Once an application reaches the `Published` state, an API resource will be created under the `<IoT_HOME>/core/repository/deployment/server/synapse-configs/default/api/` directory. Name of the file will be `{app created user}–{app name}_v{version}.xml`. This is the REST endpoint, which will be invoked once the gateway URL is accessed.

## Installing and Updating Mobile Apps

Follow the steps given under each subsection to know more on installing and updating mobile application:

### Installing Mobile Apps

Follow the steps below to install a mobile application.





You need to log in as a user with the `internal/subscriber` role to install mobile apps.





1.  Log in to the App Store using the following URL: `https://<IoTS_HOST>:9443/store/`

2.  Click on the mobile app that you wish to install, and click **Install**  
    ![image](352822389.png)
3.  Click on the preferred device in the pop-up menu to install and subscribe to the app.  
    A success message will be shown when you have successfully subscribed to the application.

4.  Optionally, you can schedule an application installation, if you have installed the system service application on your device.
    1.  Clear the **Instant Install** check box.
    2.  Enter a date and time to schedule the installation.
    3.  Click **Yes**.![image](3.png)

### Updating Mobile Apps

Follow the steps below to update a mobile application.

1.  Click on the updated version of the mobile application.  
    If you had subscribed to the previous version of the application, you will be navigated to an application page as shown in the example.  
    Example:  
    ![image](352822399.png)
2.  Click **Update**.
3.  Optionally, you can schedule an application update, if you have [installed the system service application](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application) on your device.

    1.  Clear the **Instant Update** check box.
    2.  Enter a date and time to schedule the installation.
    3.  Click **Yes**.![image](/3.png)

    ## Rating, Liking and Sorting Applications

    App Store Web interface facilitates social interaction among the users by allowing them to add application reviews, rate applications, and like or dislike reviews. Subscribed users can add ratings and reviews to an application, and those reviews are used to sort applications in App Store based on popularity.

### Adding reviews and ratings to an application

Your review will be visible to other users, together with your username, and the date that you posted the review. Follow the instructions below to add a review and to rate an application.

1.  Log in to App Store using one of the following URL: https://localhost:9443/store
2.  Click on the application to which, you need to add ratings and reviews.
3.  Click **User Reviews**.
4.  Enter your review, and click on the respective star to rate the application. For example, if you click the 4th star, you are rating the application as a 4 star application. The application rating will depict the overall average rating of the application.
5.  Click **Post Review**.  
    ![image](352822427.png)

### Liking or disliking a review

App Publisher users are allowed to add likes and dislikes to reviews posted for a particular application. Follow the instructions below to like or dislike a review.

1.  Log in to App Store using one of the following URL: https://localhost:9443/store
2.  Click on the application of which, you need to view ratings and reviews.
3.  Click **User Reviews**.
4.  Click like or dislike flags of the reviews as shown below.  
    ![image](352822416.png)

### Sorting applications 

The applications can be sorted based on user reviews, ratings, likes, and dislikes. Follow the instructions below to sort applications by popularity.

1.  Log in to App Store using one of the following URLs: https://localhost:9443/store
2.  Click **Web Applications**.
3.  Use the following set of icons to  sort the applications list based on popularity, alphabetical order, created data, and usage.   
    ![image](352822432.png)
