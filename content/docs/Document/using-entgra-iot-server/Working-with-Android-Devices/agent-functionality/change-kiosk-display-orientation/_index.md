# Change Kiosk display orientation

This section details the steps required to follow in order to perform orientation lock on enrolled Kiosk devices. This is done via the policy management system in Entgra IoTS.


## Setup Kiosk Orientation lock policy

1. Go to the **Policy Management** view from the Top Menu

    ![image](1.png)

2. In the next page select **Add New Policy**
3. Creating a new policy comprises of several steps. The first of which is to select the platform. In the next page select **Android** as the platform.

    ![image](2.png)

4. The next step involves **Profile Configuration**. It comprises of several different options. Scroll down until you find the **COSU Profile Configuration** and toggle the switch on the upper right corner to activate it.

    ![image](3.png)

5. The option **Device Global Configuration** would be unticked by default. Tick it so that additional options will be listed below it.

    ![image](4.png)

6. Scroll down till you find the field titled; **Device display orientation**. Set it to *Auto* in the drop down.
Press **Continue** to move into subsequent steps.

    ![image](5.png)

7. Edit the following steps accordingly.
8. Finally, once all settings are satisfactory, press **Save & Publish** to execute the policy onto the enrolled devices.


    ![image](6.png)

This concludes the Kiosk orientation lock policy setup procedure.
