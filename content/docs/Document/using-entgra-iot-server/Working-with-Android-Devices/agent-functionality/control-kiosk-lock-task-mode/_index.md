---
weight: 2
---

# Control Kiosk Lock Task Mode

The following section details the process by which **Lock Task mode** can be toggled on or off on enrolled Kiosk devices.

Lock Task mode allows devices to be locked into a single or set of predefined apps.

1. Go to the **Device List page**
2. Select the particular Kiosk device of choice
3. Head over to the section with the **Device Operations**, it should appear as follows;

    ![image](1.png)

4. Select the **Change LockTask** option.
5. Wait a couple of seconds for the operation to run on the device.
6. Check Entgra IoTS logs in case there is a prolonged delay or unexpeted error.