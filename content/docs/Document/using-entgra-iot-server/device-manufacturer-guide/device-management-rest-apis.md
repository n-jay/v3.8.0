# Device Management REST APIs


This section illustrates the Device Management APIs that are used in Entgra IoT Server. Follow the steps given below to access the API documentation.





If you want to try the APIs out on your environment, make sure to generate the access token. It is needed to invoke the APIs. For more information, see **[Generating the Access Token](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/device-manufacturer-guide/device-management-rest-apis/#generating-the-access-token)**.





Click the links given below to access, try out the APIs. and know more about them.





*   Make sure you enter the required values when trying out the POST or PUT APIs.
*   Do not delete any resources that are in the system by default. If you wish to try out a DELETE API, you need to add a similar resource and delete that resource.









Before trying out the APIs, click** Authorize** on the menu bar, select the **write:everything** permission, and click **Authorize** to complete the step.  
Example:  
![image](352822295.png)





*   **[Device Management Admin APIs](https://docs.wso2.com/display/IoTS320/swagger/?url=https://docs.wso2.com/display/IoTS320/swagger/device-mgt.json)**
*   **[Android APIs](https://docs.wso2.com/display/IoTS320/swagger/?url=https://docs.wso2.com/display/IoTS320/swagger/android.json)**
*   **[Windows APIs](https://docs.wso2.com/display/IoTS320/swagger/?url=https://docs.wso2.com/display/IoTS320/swagger/windows.json)**
*   **[iOS APIs](https://docs.wso2.com/display/IoTS320/swagger/?url=https://docs.wso2.com/display/IoTS320/swagger/ios.json)**

*   **[Certificate Management APIs](https://docs.wso2.com/display/IoTS320/swagger/?url=https://docs.wso2.com/display/IoTS320/swagger/certificate-mgt.json)**

# Generating the Access Token

Entgra IoT Server APIs are protected through OAuth. Therefore, you need to generate an OAuth token to invoke the APIs. The authorization and access controls are controlled through the API scopes. Follow the steps given below to obtain the access token and renew it one's it expires.

## Obtaining the access token

You can obtain an access token by providing the resource owner's username and password as an authorization grant. It requires the base64 encoded string of the `consumer-key:consumer-secret` combination. Let's take a look at how it's done.

1.  Encode the client credentials as follows:

    

    `echo -n <USERNAME>:<PASSWORD> | base64`

    Example:

    `echo -n admin:admin | base64`

    The response:

    `YWRtaW46YWRtaW4=`

    

2.  Generate the Client ID and the Secret ID.

    

    `curl -k -X POST https://<IOTS_HOST>:8243/api-application-registration/register -H 'authorization: Basic <BASE 64 ENCODED USERNAME:PASSWORD>' -H 'content-type: application/json' -d '{ "applicationName":"appName", "tags":["device_management"]}'`

    

    

    *   The base 64 encoded `USERNAME` `:PASSWORD` must be the username and password that you use to sign in to Entgra IoT Server. Else, you will not be able to get the `client_id` and `client_secret` as the response.
    *   The APIs that fall under different categories are grouped using tags. You subscribe to the API group by the tag you define in the cURL command.  
        For example, the `device_management` tag is used to group all the device management APIs including those that belong to the device type APIs.  
        To know about the available tags and the APIs grouped under each tag, navigate to the [API Cloud Store](https://api.cloud.wso2.com/store/?tenant=carbon.super), click on the available tags in the left side panel.

    

    

    The response:

    `{"client_secret":"xxxxxxxxxxxxxxxxxxxx","client_id":"xxxxxxxxxxxxxxxxxxxx"}`

    

    Example:

    

    `curl -k -X POST https://localhost:8243/api-application-registration/register -H 'authorization: Basic YWRtaW46YWRtaW4=' -H 'content-type: application/json' -d '{ "applicationName":"appName", "tags":["device_management"]}'`

    The response:

    `{"client_secret":"nboXPDTm9S1cK1yPbhAvJenbbzsa","client_id":"Ad9iV9VJ9EwyujpmLVzCi59rX8Aa"}`

    

3.  Encode the client credentials as follows:

    

    `echo -n <CLIENT_ID>:<CLIENT_SECRET> | base64`

    Example:

    `echo -n f8fc0aI14DPrQ_DwkpSau1LGdwAa:p8g_rFXtbPjl5pGMJe4bNd5fwSEa | base64`

    The response:

    `cDhnX3JGWHRiUGpsNXBHTUplNGJOZDVmd1NFYTpmOGZjMGFJMTREUHJRX0R3a3BTYXUxTEdkd0Fh`

    

4.  Generate the access token using the following command:

    

    `curl -v -k -d "grant_type=password&username=<USERNAME>&password=<PASSWORD>&scope=perm:sign-csr perm:admin:devices:view perm:admin:topics:view perm:roles:add perm:roles:add-users perm:roles:update perm:roles:permissions perm:roles:details perm:roles:view perm:roles:create-combined-role perm:roles:delete perm:dashboard:vulnerabilities perm:dashboard:non-compliant-count perm:dashboard:non-compliant perm:dashboard:by-groups perm:dashboard:device-counts perm:dashboard:feature-non-compliant perm:dashboard:count-overview perm:dashboard:filtered-count perm:dashboard:details perm:get-activity perm:devices:delete perm:devices:applications perm:devices:effective-policy perm:devices:compliance-data perm:devices:features perm:devices:operations perm:devices:search perm:devices:details perm:devices:update perm:devices:view perm:view-configuration perm:manage-configuration perm:policies:remove perm:policies:priorities perm:policies:deactivate perm:policies:get-policy-details perm:policies:manage perm:policies:activate perm:policies:update perm:policies:changes perm:policies:get-details perm:users:add perm:users:details perm:users:count perm:users:delete perm:users:roles perm:users:user-details perm:users:credentials perm:users:search perm:users:is-exist perm:users:update perm:users:send-invitation perm:admin-users:view perm:groups:devices perm:groups:update perm:groups:add perm:groups:device perm:groups:devices-count perm:groups:remove perm:groups:groups perm:groups:groups-view perm:groups:share perm:groups:count perm:groups:roles perm:groups:devices-remove perm:groups:devices-add perm:groups:assign perm:device-types:features perm:device-types:types perm:applications:install perm:applications:uninstall perm:admin-groups:count perm:admin-groups:view perm:notifications:mark-checked perm:notifications:view perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:view perm:admin:certificates:add perm:admin:certificates:verify perm:admin perm:devicetype:deployment perm:device-types:events perm:device-types:events:view perm:admin:device-type perm:device:enroll perm:geo-service:analytics-view perm:geo-service:alerts-manage perm:sign-csr perm:admin:devices:view perm:roles:add perm:roles:add-users perm:roles:update perm:roles:permissions perm:roles:details perm:roles:view perm:roles:create-combined-role perm:roles:delete perm:get-activity perm:devices:delete perm:devices:applications perm:devices:effective-policy perm:devices:compliance-data perm:devices:features perm:devices:operations perm:devices:search perm:devices:details perm:devices:update perm:devices:view perm:view-configuration perm:manage-configuration perm:policies:remove perm:policies:priorities perm:policies:deactivate perm:policies:get-policy-details perm:policies:manage perm:policies:activate perm:policies:update perm:policies:changes perm:policies:get-details perm:users:add perm:users:details perm:users:count perm:users:delete perm:users:roles perm:users:user-details perm:users:credentials perm:users:search perm:users:is-exist perm:users:update perm:users:send-invitation perm:admin-users:view perm:groups:devices perm:groups:update perm:groups:add perm:groups:device perm:groups:devices-count perm:groups:remove perm:groups:groups perm:groups:groups-view perm:groups:share perm:groups:count perm:groups:roles perm:groups:devices-remove perm:groups:devices-add perm:groups:assign perm:device-types:features perm:device-types:types perm:applications:install perm:applications:uninstall perm:admin-groups:count perm:admin-groups:view perm:notifications:mark-checked perm:notifications:view perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:view perm:admin:certificates:add perm:admin:certificates:verify perm:ios:enroll perm:ios:view-device perm:ios:apn perm:ios:ldap perm:ios:enterprise-app perm:ios:store-application perm:ios:remove-application perm:ios:app-list perm:ios:profile-list perm:ios:lock perm:ios:enterprise-wipe perm:ios:device-info perm:ios:restriction perm:ios:email perm:ios:cellular perm:ios:applications perm:ios:wifi perm:ios:ring perm:ios:location perm:ios:notification perm:ios:airplay perm:ios:caldav perm:ios:cal-subscription perm:ios:passcode-policy perm:ios:webclip perm:ios:vpn perm:ios:per-app-vpn perm:ios:app-to-per-app-vpn perm:ios:app-lock perm:ios:clear-passcode perm:ios:remove-profile perm:ios:get-restrictions perm:ios:wipe-data perm:admin perm:android:enroll perm:android:wipe perm:android:ring perm:android:lock-devices perm:android:configure-vpn perm:android:configure-wifi perm:android:enroll perm:android:uninstall-application perm:android:manage-configuration perm:android:location perm:android:install-application perm:android:mute perm:android:change-lock-code perm:android:blacklist-applications perm:android:set-password-policy perm:android:encrypt-storage perm:android:clear-password perm:android:enterprise-wipe perm:android:info perm:android:view-configuration perm:android:upgrade-firmware perm:android:set-webclip perm:android:send-notification perm:android:disenroll perm:android:update-application perm:android:unlock-devices perm:android:control-camera perm:android:reboot perm:android:logcat perm:device:enroll perm:device:disenroll perm:device:modify perm:device:operations perm:device:publish-event" -H "Authorization: Basic <BASE 64 ENCODEd CLIENT_ID:CLIENT_SECRET>" -H "Content-Type: application/x-www-form-urlencoded" https://<IOTS_HOST>:8243/token`

    

    

    The permission to invoke the APIs are assigned via the scope defined in each API. You can define all the scopes to generate an access token so you can invoke all the APIs or you can generate an access token that only has the required scope to invoke a specific API.

    *   For more information on all the device management API scopes, see [Device Management API Scopes](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/tutorials/getting-started-with-apis/device-management-api-scopes/).
    *   For more information on The API scope needeD to invoke an API, see [Getting the Scope Details of an API](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/tutorials/getting-started-with-apis/getting-the-scope-details-of-an-api/).


    

    

    

    Example:

    

    Generate the access token for the user having the username `admin` and password `admin`, and using the default Entgra IoT Server host, which is `localhost`. In this example, we are generating an access token that has access to all the device management scopes.

    `curl -v -k -d "grant_type=password&username=admin&password=admin&scope=perm:sign-csr perm:admin:devices:view perm:admin:topics:view perm:roles:add perm:roles:add-users perm:roles:update perm:roles:permissions perm:roles:details perm:roles:view perm:roles:create-combined-role perm:roles:delete perm:dashboard:vulnerabilities perm:dashboard:non-compliant-count perm:dashboard:non-compliant perm:dashboard:by-groups perm:dashboard:device-counts perm:dashboard:feature-non-compliant perm:dashboard:count-overview perm:dashboard:filtered-count perm:dashboard:details perm:get-activity perm:devices:delete perm:devices:applications perm:devices:effective-policy perm:devices:compliance-data perm:devices:features perm:devices:operations perm:devices:search perm:devices:details perm:devices:update perm:devices:view perm:view-configuration perm:manage-configuration perm:policies:remove perm:policies:priorities perm:policies:deactivate perm:policies:get-policy-details perm:policies:manage perm:policies:activate perm:policies:update perm:policies:changes perm:policies:get-details perm:users:add perm:users:details perm:users:count perm:users:delete perm:users:roles perm:users:user-details perm:users:credentials perm:users:search perm:users:is-exist perm:users:update perm:users:send-invitation perm:admin-users:view perm:groups:devices perm:groups:update perm:groups:add perm:groups:device perm:groups:devices-count perm:groups:remove perm:groups:groups perm:groups:groups-view perm:groups:share perm:groups:count perm:groups:roles perm:groups:devices-remove perm:groups:devices-add perm:groups:assign perm:device-types:features perm:device-types:types perm:applications:install perm:applications:uninstall perm:admin-groups:count perm:admin-groups:view perm:notifications:mark-checked perm:notifications:view perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:view perm:admin:certificates:add perm:admin:certificates:verify perm:admin perm:devicetype:deployment perm:device-types:events perm:device-types:events:view perm:admin:device-type perm:device:enroll perm:geo-service:analytics-view perm:geo-service:alerts-manage perm:sign-csr perm:admin:devices:view perm:roles:add perm:roles:add-users perm:roles:update perm:roles:permissions perm:roles:details perm:roles:view perm:roles:create-combined-role perm:roles:delete perm:get-activity perm:devices:delete perm:devices:applications perm:devices:effective-policy perm:devices:compliance-data perm:devices:features perm:devices:operations perm:devices:search perm:devices:details perm:devices:update perm:devices:view perm:view-configuration perm:manage-configuration perm:policies:remove perm:policies:priorities perm:policies:deactivate perm:policies:get-policy-details perm:policies:manage perm:policies:activate perm:policies:update perm:policies:changes perm:policies:get-details perm:users:add perm:users:details perm:users:count perm:users:delete perm:users:roles perm:users:user-details perm:users:credentials perm:users:search perm:users:is-exist perm:users:update perm:users:send-invitation perm:admin-users:view perm:groups:devices perm:groups:update perm:groups:add perm:groups:device perm:groups:devices-count perm:groups:remove perm:groups:groups perm:groups:groups-view perm:groups:share perm:groups:count perm:groups:roles perm:groups:devices-remove perm:groups:devices-add perm:groups:assign perm:device-types:features perm:device-types:types perm:applications:install perm:applications:uninstall perm:admin-groups:count perm:admin-groups:view perm:notifications:mark-checked perm:notifications:view perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:view perm:admin:certificates:add perm:admin:certificates:verify perm:ios:enroll perm:ios:view-device perm:ios:apn perm:ios:ldap perm:ios:enterprise-app perm:ios:store-application perm:ios:remove-application perm:ios:app-list perm:ios:profile-list perm:ios:lock perm:ios:enterprise-wipe perm:ios:device-info perm:ios:restriction perm:ios:email perm:ios:cellular perm:ios:applications perm:ios:wifi perm:ios:ring perm:ios:location perm:ios:notification perm:ios:airplay perm:ios:caldav perm:ios:cal-subscription perm:ios:passcode-policy perm:ios:webclip perm:ios:vpn perm:ios:per-app-vpn perm:ios:app-to-per-app-vpn perm:ios:app-lock perm:ios:clear-passcode perm:ios:remove-profile perm:ios:get-restrictions perm:ios:wipe-data perm:admin perm:android:enroll perm:android:wipe perm:android:ring perm:android:lock-devices perm:android:configure-vpn perm:android:configure-wifi perm:android:enroll perm:android:uninstall-application perm:android:manage-configuration perm:android:location perm:android:install-application perm:android:mute perm:android:change-lock-code perm:android:blacklist-applications perm:android:set-password-policy perm:android:encrypt-storage perm:android:clear-password perm:android:enterprise-wipe perm:android:info perm:android:view-configuration perm:android:upgrade-firmware perm:android:set-webclip perm:android:send-notification perm:android:disenroll perm:android:update-application perm:android:unlock-devices perm:android:control-camera perm:android:reboot perm:android:logcat perm:device:enroll perm:device:disenroll perm:device:modify perm:device:operations perm:device:publish-event" -H "Authorization: Basic QWQ5aVY5Vko5RXd5dWpwbUxWekNpNTlyWDhBYTpuYm9YUERUbTlTMWNLMXlQYmhBdkplbmJienNh" -H "Content-Type: application/x-www-form-urlencoded" https://localhost:8243/token`

    The response:

    {
      "access_token": "168v5699-5678-rt61-1534-4a169v5u88e0",
      "refresh_token": "3trtg45-64t5-1693-gr56-6th5356r4tr5",
      "scope": "perm:admin-groups:count perm:admin-groups:view perm:admin-users:view perm:admin:certificates:add perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:verify perm:admin:certificates:view perm:admin:devices:view perm:android:blacklist-applications perm:android:change-lock-code perm:android:clear-password perm:android:configure-vpn perm:android:configure-wifi perm:android:control-camera perm:android:disenroll perm:android:encrypt-storage perm:android:enroll perm:android:enterprise-wipe perm:android:info perm:android:install-application perm:android:location perm:android:lock-devices perm:android:logcat perm:android:manage-configuration perm:android:mute perm:android:reboot perm:android:ring perm:android:send-notification perm:android:set-password-policy perm:android:set-webclip perm:android:uninstall-application perm:android:unlock-devices perm:android:update-application perm:android:upgrade-firmware perm:android:view-configuration perm:android:wipe perm:applications:install perm:applications:uninstall perm:device-types:features perm:device-types:types perm:devices:applications perm:devices:compliance-data perm:devices:delete perm:devices:details perm:devices:effective-policy perm:devices:features perm:devices:operations perm:devices:search perm:devices:update perm:devices:view perm:get-activity perm:groups:add perm:groups:assign perm:groups:count perm:groups:device perm:groups:devices perm:groups:devices-add perm:groups:devices-count perm:groups:devices-remove perm:groups:groups perm:groups:groups-view perm:groups:remove perm:groups:roles perm:groups:share perm:groups:update perm:ios:airplay perm:ios:apn perm:ios:app-list perm:ios:app-lock perm:ios:app-to-per-app-vpn perm:ios:applications perm:ios:cal-subscription perm:ios:caldav perm:ios:cellular perm:ios:clear-passcode perm:ios:device-info perm:ios:email perm:ios:enroll perm:ios:enterprise-app perm:ios:enterprise-wipe perm:ios:get-restrictions perm:ios:ldap perm:ios:location perm:ios:lock perm:ios:notification perm:ios:passcode-policy perm:ios:per-app-vpn perm:ios:profile-list perm:ios:remove-application perm:ios:remove-profile perm:ios:restriction perm:ios:ring perm:ios:store-application perm:ios:view-device perm:ios:vpn perm:ios:webclip perm:ios:wifi perm:ios:wipe-data perm:manage-configuration perm:notifications:mark-checked perm:notifications:view perm:policies:activate perm:policies:changes perm:policies:deactivate perm:policies:get-details perm:policies:get-policy-details perm:policies:manage perm:policies:priorities perm:policies:remove perm:policies:update perm:roles:add perm:roles:add-users perm:roles:create-combined-role perm:roles:delete perm:roles:details perm:roles:permissions perm:roles:update perm:roles:view perm:users:add perm:users:count perm:users:credentials perm:users:delete perm:users:details perm:users:is-exist perm:users:roles perm:users:search perm:users:send-invitation perm:users:update perm:users:user-details perm:view-configuration perm:device:enroll perm:device:disenroll perm:device:modify perm:device:operations perm:device:publish-event",
      "token_type": "Bearer",
      "expires_in": 3880
    }
    

    

    The access token you generated expires in an hour. After it expires you can generate a new access token using the refresh token as shown below.

    

    

* * *

## Generating a new access token from the refresh token

The access token expires in an hour. Let's look at how to use the refresh token to generate a new access token using the cURL command given below.



`curl -k -d "grant_type=refresh_token&refresh_token=<THE REFRESH TOKEN>&scope=PRODUCTION" -H "Authorization: Basic <BASE 64 ENCODED CLIENT_ID:CLIENT_SECRET>" -H "Content-Type: application/x-www-form-urlencoded" https://localhost:8243/token`



Example:



`curl -k -d "grant_type=refresh_token&refresh_token=3trtg45-64t5-1693-gr56-6th5356r4tr5&scope=PRODUCTION" -H "Authorization: Basic cDhnX3JGWHRiUGpsNXBHTUplNGJOZDVmd1NFYTpmOGZjMGFJMTREUHJRX0R3a3BTYXUxTEdkd0Fh" -H "Content-Type: application/x-www-form-urlencoded" https://localhost:8243/token`

The response:

`{"access_token":"237bfc64-9567-3edf-9ad6-3795d37fa368","refresh_token":"62e597b3-0960-39c0-894f-7056fcdf2dc6","scope":"perm:admin-groups:count perm:admin-groups:view perm:admin-users:view perm:admin:certificates:add perm:admin:certificates:delete perm:admin:certificates:details perm:admin:certificates:verify perm:admin:certificates:view perm:admin:devices:view perm:android:blacklist-applications perm:android:change-lock-code perm:android:clear-password perm:android:configure-vpn perm:android:configure-wifi perm:android:control-camera perm:android:disenroll perm:android:encrypt-storage perm:android:enroll perm:android:enterprise-wipe perm:android:info perm:android:install-application perm:android:location perm:android:lock-devices perm:android:logcat perm:android:manage-configuration perm:android:mute perm:android:reboot perm:android:ring perm:android:send-notification perm:android:set-password-policy perm:android:set-webclip perm:android:uninstall-application perm:android:unlock-devices perm:android:update-application perm:android:upgrade-firmware perm:android:view-configuration perm:android:wipe perm:applications:install perm:applications:uninstall perm:device-types:features perm:device-types:types perm:devices:applications perm:devices:compliance-data perm:devices:delete perm:devices:details perm:devices:effective-policy perm:devices:features perm:devices:operations perm:devices:search perm:devices:update perm:devices:view perm:get-activity perm:groups:add perm:groups:assign perm:groups:count perm:groups:device perm:groups:devices perm:groups:devices-add perm:groups:devices-count perm:groups:devices-remove perm:groups:groups perm:groups:groups-view perm:groups:remove perm:groups:roles perm:groups:share perm:groups:update perm:manage-configuration perm:notifications:mark-checked perm:notifications:view perm:policies:activate perm:policies:changes perm:policies:deactivate perm:policies:get-details perm:policies:get-policy-details perm:policies:manage perm:policies:priorities perm:policies:remove perm:policies:update perm:roles:add perm:roles:add-users perm:roles:create-combined-role perm:roles:delete perm:roles:details perm:roles:permissions perm:roles:update perm:roles:view perm:users:add perm:users:count perm:users:credentials perm:users:delete perm:users:details perm:users:is-exist perm:users:roles perm:users:search perm:users:send-invitation perm:users:update perm:users:user-details perm:view-configuration perm:device:enroll perm:device:disenroll perm:device:modify perm:device:operations perm:device:publish-event","token_type":"Bearer","expires_in":3600}`
