---
bookCollapseSection: true
weight: 6
---
# Entgra IoT Server Security

This section lists all the concepts, tutorials, how-to guides, and deployment guides related to Entgra IoT Server security.

### **Concepts**

*   [Application-level Security](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#application-level-security)
    *   [Authentication](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#authentication): [Single Sign-On (SSO)](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#single-sign-on)
    *   [Authorization](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#authorization): [Role-based Access Control (RBAC)](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#role-based-access-control), [Scopes](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#scopes)
    *   [Encryption](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#encryption)
    *   [Certificates](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#certificates): [Mutual SSL](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication)
*   [Transport-level Security](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#transport-level-security)

### **Tutorials**

*   [Obtaining Access Tokens](https://docs.wso2.com/display/IoTS310/Getting+Started+with+APIs#GettingStartedwithAPIs-Obtainingtheaccesstoken)
*   [Regenerating Access Tokens](https://docs.wso2.com/display/IoTS310/Getting+Started+with+APIs#GettingStartedwithAPIs-Generatinganewaccesstokenfromtherefreshtoken)
*   [Getting Scope Details of an API](https://docs.wso2.com/display/IoTS310/Getting+the+Scope+Details+of+an+API)

### How-To Guides

*   [Resetting a User Password](https://docs.wso2.com/display/IoTS310/Resetting+a+User+Password)
*   [Changing the Super Administrator Username and Password](https://docs.wso2.com/display/IoTS310/Changing+the+Super+Administrator+Username+and+Password)
*   [Managing Client Side Mutual SSL Certificates](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication)
*   [Generating Certificates from the Apple Developer Portal](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/working-with-ios/ios-configurations/#generating-certificates-from-the-apple-developer-portal)
*   [Adding Custom Grant Types](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/entgra-iot-server-security/adding-custom-grant-types/)
*   [Configuring Keystores in Entgra IoT Server](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/Configuring-Keystores-in-Entgra-IoT-Server/)

### Deployment Guides

*   [Integrating a Third-Party Identity Provider for Access Token Management](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/entgra-iot-server-security/intergrating-third-party-identity/)
