# Adding Custom Grant Types

Entgra IoT Server uses the OAuth 2.0 standard for [authorization](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#authorization) and supports the following seven grant types by default:

*   Authorization code grant
*   Client credentials grant
*   Password grant
*   Refresh token grant
*   SAML2 bearer grant
*   NTLM grant
*   JWT grant

It also has the flexibility to support custom grant types. This section explains how you can implement a custom grant type and demonstrates a sample.

### Implementation

Follow the steps below to implement a new grant type:



Before you begin



Entgra IoT Server is deployable in both standalone and clustered setups. If you wish to deploy Entgra IoT Server in a clustered setup with WSO2 Identity Server as the key manager, first [Download WSO2 Identity Server](https://wso2.com/identity-and-access-management#download) and unzip it. 

*   If the deployment is standalone, the `<PRODUCT_HOME>` refers to the home directory of Entgra IoT Server.
*   If the deployment is clustered where the WSO2 Identity Server acts as the key manager, the `<PRODUCT_HOME>` refers to the home directory of WSO2 Identity Server.





1.  Implement the following two extensions:
    1.  `**GrantTypeHandler**`: This is the implementation of the grant type. It is used to define the grant type validation and [token](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/key-concepts/#tokens) issuance mechanisms.  You can write the new implementation by implementing the `AuthorizationGrantHandler` interface or by extending `AbstractAuthorizationGrantHandler`. In most cases, it is enough to extend the `AbstractAuthorizationGrantHandler` in the WSO2 OAuth component.
    2.  **`GrantTypeValidator`**: This is used to validate the grant request sent to the `/token` endpoint. You can define the parameters that must be in the request and how they are validated. You can write the new implementation by extending the `AbstractValidator` in Apache Amber component. 
2.  Package the class as a `.jar` file and place it in the `<PRODUCT_HOME>/lib` directory.
3.  Register the custom grant type by adding a new entry between the <OAuth><SupportedGrantTypes> element of the `<PRODUCT_HOME>/conf/identity/identity.xml` file.

    
    <SupportedGrantType>
        <GrantTypeName>grant type identifier </GrantTypeName>
        <GrantTypeHandlerImplClass>full qualified class name of grant handler</GrantTypeHandlerImplClass>
        <GrantTypeValidatorImplClass>full qualified class name of grant validator</GrantTypeValidatorImplClass>
    </SupportedGrantType>

    

    

    Add a unique identifier between the <`GrantTypeName></`GrantTypeName>`` tags.

    

    

Next, try out the sample below to test this out.

### Sample

This sample demonstrates defining a new grant type called mobile. The mobile grant type is similar to the password grant type except for passing a mobile number instead of a password. 

Let's get started.

1.  Obtain the grant type sample project by following either of the steps below:

    1.  Download the **.jar** file from [here](https://docs.wso2.com/download/attachments/60493976/custom-grant-1.0.0.jar?version=1&modificationDate=1452601970000&api=v2).
    2.  Download the buildable source file from [here](https://docs.wso2.com/download/attachments/60493976/custom-grant.zip?version=1&modificationDate=1452601958000&api=v2). 

        

        

        The grant handler and validator class is inside the `org.wso2.sample.identity.oauth2.grant.mobile` package. You can modify it as required.

        

        

2.  If you downloaded the source file, navigate to the sample's location through command prompt and generate a `.jar` file. 

    --Navigating to the sample directory----------------------
    cd <Sample location> 

    --Generating the .jar file--------------------------------
    mvn clean install

3.  Copy the `.jar` file into the `<IOT_HOME>/lib` directory.

    

    

    You can also modify the project and build it using Apache Maven 3.

    

    

4.  Configure the following in the `<IOT_HOME>/conf/identity/identity.xml` file under the `<OAuth><SupportedGrantTypes>` element. 

    <SupportedGrantType>
        <GrantTypeName>mobile</GrantTypeName>
        <GrantTypeHandlerImplClass>org.wso2.sample.identity.oauth2.grant.mobile.MobileGrant</GrantTypeHandlerImplClass>
        <GrantTypeValidatorImplClass>org.wso2.sample.identity.oauth2.grant.mobile.MobileGrantValidator</GrantTypeValidatorImplClass>
    </SupportedGrantType>

5.  Restart the server if you have started it before. 

    

    

    In a **clustered setup** with WSO2 Identity Server used as the key manager, make sure to restart the Entgra IoT Server and the WSO2 Identity Server.

    

    

    --Running Entgra IoT Server on Linux/MacOS/Solaris-------
    sh iot-server.sh

    --Running Entgra IoT Server on Windows-------------------
    iot-server.bat

    --Running WSO2 Identity Server on Linux/MacOS/Solaris--
    sh wso2server.sh

    --Running WSO2 Identity Server on Windows--------------
    wso2server.bat

6.  Generate an OAuth client key and an OAuth client secret. 

7.  Send the grant request to the `/token` API using a cURL command.

    1.  Add the `grant_type=mobile` and `mobileNumber` parameters to the HTTP POST body.  
        Example: 

        `grant_type=mobile&mobileNumber=0333444`

    2.  Replace the `clientid:clientsecret` with the **OAuth Client Key** and **OAuth Client Secret** and run the following sample cURL command in the command prompt. 

        `curl --user clientid:clientsecret -k -d "grant_type=mobile&mobileNumber=0333444" -H "Content-Type: application/x-www-form-urlencoded" https://localhost:9443/oauth2/token`

        The system displays the following JSON response with an access token.

        `{"token_type":"bearer","expires_in":2823,"refresh_token":"26e1ebf16cfa4e67c3bf39d72d5c276","access_token":"d9ef87802a22cf7682c2e77df72c735"}`
