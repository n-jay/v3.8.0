# Setting Up A Federated IdP with OpenID Connect

This document covers the steps on how an external Identity Server is used to authenticate users that log in to Entgra IoT Server using the OpenID Connect protocol. To make it easy for you to try out the scenario, the IdentityServer4 that is hosted on `http://demo.identityserver.io` is used. You can follow the same steps to configure another Identity server.

Let's get started! 

### Installing OpenID Authenticator Feature in IoT Server

To add the OpenID Connected authenticator to Entgra IoT Server, you need to install `org.wso2.carbon.identity.application.authenticator.oidc.server.feature`.

Follow the steps given below to install the OpenID Connect application authenticator using the Maven execution script.

1.  Create a file named `openid-extension-deployer.xml` in the `<IOTS_HOME>` directory.
2.  Copy the configurations given below to the `openid-extension-deployer.xml` file you created.

    

    

    

    

    **openid-extension-deployer.xml**


    <!--
     ~ Copyright (c) 2017, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
     ~
     ~ WSO2 Inc. licenses this file to you under the Apache License,
     ~ Version 2.0 (the "License"); you may not use this file except
     ~ in compliance with the License.
     ~ You may obtain a copy of the License at
     ~
     ~ http://www.apache.org/licenses/LICENSE-2.0
     ~
     ~ Unless required by applicable law or agreed to in writing,
     ~ software distributed under the License is distributed on an
     ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     ~ KIND, either express or implied. See the License for the
     ~ specific language governing permissions and limitations
     ~ under the License.
     -->
    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
       <parent>
          <groupId>org.wso2</groupId>
          <artifactId>wso2</artifactId>
          <version>1</version>
       </parent>
       <modelVersion>4.0.0</modelVersion>
       <groupId>org.wso2.iot.devicemgt-plugins</groupId>
       <artifactId>openid-extension-script</artifactId>
       <version>3.1.0</version>
       <packaging>pom</packaging>
       <name>OpenID Extension Script</name>
       <url>http://wso2.org</url>
       <build>
          <plugins>
             <plugin>
                <groupId>org.wso2.maven</groupId>
                <artifactId>carbon-p2-plugin</artifactId>
                <version>1.5.4</version>
                <executions>
                   <execution>
                      <id>2-p2-repo-generation</id>
                      <phase>package</phase>
                      <goals>
                         <goal>p2-repo-gen</goal>
                      </goals>
                      <configuration>
                         <metadataRepository>file:${basedir}/p2-repo</metadataRepository>
                         <artifactRepository>file:${basedir}/p2-repo</artifactRepository>
                         <publishArtifacts>true</publishArtifacts>
                         <publishArtifactRepository>true</publishArtifactRepository>
                         <featureArtifacts>
                            <featureArtifactDef>org.wso2.carbon.identity.outbound.auth.oidc:org.wso2.carbon.identity.application.authenticator.oidc.server.feature:${identity-application-auth-oidc.version}</featureArtifactDef>
                         </featureArtifacts>
                      </configuration>
                   </execution>
                   <execution>
                      <id>default-feature-install</id>
                      <phase>package</phase>
                      <goals>
                         <goal>p2-profile-gen</goal>
                      </goals>
                      <configuration>
                         <profile>default</profile>
                         <metadataRepository>file:${basedir}/p2-repo</metadataRepository>
                         <artifactRepository>file:${basedir}/p2-repo</artifactRepository>
                         <destination>${basedir}/wso2/components</destination>
                         <deleteOldProfileFiles>false</deleteOldProfileFiles>
                         <features>
                            <feature>
                               <id>org.wso2.carbon.identity.application.authenticator.oidc.server.feature.group</id>
                               <version>${identity-application-auth-oidc.version}</version>
                            </feature>
                         </features>
                      </configuration>
                   </execution>
                </executions>
             </plugin>
             <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.6</version>
                <executions>
                   <execution>
                      <id>default-feature-install</id>
                      <phase>package</phase>
                      <configuration>
                         <target>
                            <replaceregexp file="./wso2/components/default/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info" match="(org.wso2.carbon.identity.application.authenticator.oidc.*)false" replace="\1true" byline="true" />
                         </target>
                      </configuration>
                      <goals>
                         <goal>run</goal>
                      </goals>
                   </execution>
                </executions>
             </plugin>
          </plugins>
       </build>
       <pluginRepositories>
          <pluginRepository>
             <id>wso2.releases</id>
             <name>WSO2 internal Repository</name>
             <url>http://maven.wso2.org/nexus/content/repositories/releases/</url>
             <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
             </releases>
          </pluginRepository>
          <pluginRepository>
             <id>wso2.snapshots</id>
             <name>Apache Snapshot Repository</name>
             <url>http://maven.wso2.org/nexus/content/repositories/snapshots/</url>
             <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
             </snapshots>
             <releases>
                <enabled>false</enabled>
             </releases>
          </pluginRepository>
          <pluginRepository>
             <id>wso2-nexus</id>
             <name>WSO2 internal Repository</name>
             <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
             <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
             </releases>
          </pluginRepository>
       </pluginRepositories>
       <repositories>
          <!-- Before adding ANYTHING in here, please start a discussion on the dev list.
                Ideally the Axis2 build should only use Maven central (which is available
                by default) and nothing else. We had troubles with other repositories in
                the past. Therefore configuring additional repositories here should be
                considered very carefully. -->
          <repository>
             <id>wso2-nexus</id>
             <name>WSO2 internal Repository</name>
             <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
             <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
             </releases>
          </repository>
          <repository>
             <id>wso2.releases</id>
             <name>WSO2 internal Repository</name>
             <url>http://maven.wso2.org/nexus/content/repositories/releases/</url>
             <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
             </releases>
          </repository>
          <repository>
             <id>wso2.snapshots</id>
             <name>WSO2 Snapshot Repository</name>
             <url>http://maven.wso2.org/nexus/content/repositories/snapshots/</url>
             <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
             </snapshots>
             <releases>
                <enabled>false</enabled>
             </releases>
          </repository>
       </repositories>
       <properties>
          <identity-application-auth-oidc.version>5.1.8</identity-application-auth-oidc.version>
       </properties>
    </project>

    

    

    

3.  Navigate to the `<IOTS_HOME>` directory via the terminal.

    `cd <IOTS_HOME>`

4.  Execute the Maven script to install the `org.wso2.carbon.identity.application.authenticator.oidc.server.feature`.

    `mvn clean install -f openid-extension-deployer.xml`

### Configuring the Entgra IoT Server IdP

You need to add the federated OpenID Connect authentication configurations to the Entgra IoT Server's default Identity Provider (IdP) configurations. Follow the steps given below:

1.  Open the `<IOTS_HOME>/conf/identity/identity-providers/iot_default.xml` file.
2.  Add the following configuration before the `<FederatedAuthenticatorConfigs>` tag.

    `<IsEnabled>true</IsEnabled>`

3.  Add the following configurations inside the `<FederatedAuthenticatorConfigs>` tag.

    

    

    The IdentiyServer4's hosted demo server is available at `http://demo.identityserver.io`. If you are using your own external Identity Server, make sure to replace `http://demo.identityserver.io` with the URL of your hosted Identity Server.

    

    

    <OpenIDConnectFederatedAuthenticatorConfig>
       <Name>OpenIDConnectAuthenticator</Name>
       <DisplayName>openidconnect</DisplayName>
       <IsEnabled>true</IsEnabled>
       <Properties>
          <Property>
             <Name>ClientId</Name>
             <Value>server.code</Value>
          </Property>
          <Property>
             <Name>ClientSecret</Name>
             <Value>secret</Value>
          </Property>
          <Property>
             <Name>OAuth2AuthzEPUrl</Name>
             <Value>http://demo.identityserver.io/connect/authorize</Value>
          </Property>
          <Property>
             <Name>OAuth2TokenEPUrl</Name>
             <Value>http://demo.identityserver.io/connect/token</Value>
          </Property>
          <Property>
             <Name>callbackUrl</Name>
             <Value>https://localhost:9443/commonauth</Value>
          </Property>
          <Property>
             <Name>IsUserIdInClaims</Name>
             <Value>false</Value>
          </Property>
          <Property>
             <Name>commonAuthQueryParams</Name>
             <Value>scope=openid</Value>
          </Property>
       </Properties>
    </OpenIDConnectFederatedAuthenticatorConfig>

4.  Add `OpenIDConnectAuthenticator` as the value for the `<DefaultAuthenticatorConfig>` tag.  
    Example:

    `<DefaultAuthenticatorConfig>OpenIDConnectAuthenticator</DefaultAuthenticatorConfig>`

    

    Note!

    

    Make sure that the above configuration is in one line as shown above. If it is broken into new lines as shown below, you run into errors.

    **Incorrect format**

    <DefaultAuthenticatorConfig>
    OpenIDConnectAuthenticator
    </DefaultAuthenticatorConfig>

    

    

5.  Add the following configurations inside the `<ClaimConfig>` tag to get the role of the user that is signing in because the demo server does not return any values.

    <ClaimMappings>
       <ClaimMapping>
          <RemoteClaim>
             <ClaimUri>idp</ClaimUri>
          </RemoteClaim>
          <LocalClaim>
             <ClaimUri>http://wso2.org/claims/role</ClaimUri>
          </LocalClaim>
          <DefaultValue/>
       </ClaimMapping>
    </ClaimMappings>

6.  Add the following configurations inside the `<PermissionAndRoleConfig>` tag. The new users that get created on the fly via Just In Time (JIT) provisioning is assigned the `Internal/devicemgt-admin` role by default.

    

    

    This allows you to map a remote role name, which is as a role that is not defined in Entgra IoT Server, into a local role name, which is a role that is defined in Entgra IoT Server. In the example given below, the role that is passed from IdentityServer4 to the `idp` claim is mapped to the `Internal/devicemgt-admin` role of Entgra IoT Server. You can use this feature to map remote roles, such as mapping the `Administrator` into the `Internal/devicemgt-admin` role.

    

    

    <RoleMappings>
       <RoleMapping>
          <localRole>
             <LocalRoleName>Internal/devicemgt-admin</LocalRoleName>
             <UserStoreId>PRIMARY</UserStoreId>
          </localRole>
          <remoteRole>local</remoteRole>
       </RoleMapping>
    </RoleMappings>

7.  Add the following configurations inside the `<JustInTimeProvisioningConfig>` tag to enable JIT provisioning.

    `<IsProvisioningEnabled>true</IsProvisioningEnabled>`

    

    What is JIT provisioning?

    

    With JIT provisioning, you can create users on the fly the first time they try to log in to Entgra IoT Server.

    

    

### Configuring the Service Providers

The Jaggery applications in Entgra IoT Server, such as the device management, app store, and app publisher consoles, are configured as service providers to enable Single Sign-On (SSO) and authentication. For more information, on accessing the different consoles available in Entgra IoT Server, see [Accessing the Entgra IoT Server Consoles](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-consoles).


You need to configure the service providers to have both the basic authentication mechanism that authenticates the user based on the username and password that was entered, and to have the OpenID Connect authentication mechanism.

For this use case, only the devicemgt service provider is configured. If you want to configure the other applications, you need to add the same configurations that are given below to the other service providers in the `<IOTS_HOME>/conf/identity/service-providers` directory.

1.  Open the `<IOTS_HOME>/conf/identity/service-providers/devicemgt.xml` file.
2.  Add the following configurations after the `<StepOrder>1</StepOrder>` property.

    <LocalAuthenticatorConfigs>
       <LocalAuthenticatorConfig>
          <Name>BasicAuthenticator</Name>
          <DisplayName>basicauth</DisplayName>
          <IsEnabled>true</IsEnabled>
       </LocalAuthenticatorConfig>
    </LocalAuthenticatorConfigs>
    <FederatedIdentityProviders>
       <IdentityProvider>
          <IdentityProviderName>wso2.org/products/iot</IdentityProviderName>
          <IsEnabled>true</IsEnabled>
          <DefaultAuthenticatorConfig>
             <FederatedAuthenticatorConfig>
                <Name>FacebookAuthenticator</Name>
                <IsEnabled>true</IsEnabled>
             </FederatedAuthenticatorConfig>
          </DefaultAuthenticatorConfig>
       </IdentityProvider>
    </FederatedIdentityProviders>

### Extend the log-in page for OpenID Connect

You need to extend the device management console's log in page so that it supports both the basic authentication, which is logging in using the username and password, and the OpenID connect authentication options.

Follow the steps given below:

1.  Create a new folder named `company.page.sign-in` in the `<IOTS_HOME>/` `repository/deployment/server/jaggeryapps/devicemgt/app/pages` directory.
2.  Create a new file named `sign-in.hbs` in the `company.page.sign-in` directory you just created and copy the configurations given below to the file.

    

    

    

    

    {% raw %}
    {{!
     Copyright (c) 2018, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
     WSO2 Inc. licenses this file to you under the Apache License,
     Version 2.0 (the "License"); you may not use this file except
     in compliance with the License.
     You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
     Unless required by applicable law or agreed to in writing,
     software distributed under the License is distributed on an
     "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     KIND, either express or implied. See the License for the
     specific language governing permissions and limitations
     under the License.
    }}
    {% raw %}           
    {{#zone "title"}}
    {{! to override parent page title }}
    {{/zone}}  
    {{unit "cdmf.unit.ui.title" pageTitle="Login"}}
    {{#zone "content"}}
    {% endraw %}
     <p class="page-sub-title">Login</p>
               <hr />
               {% raw %}
               {{#if message}}
                   
                       <i class="icon fw fw-warning"></i> {{message}}!           
               {{/if}}
               
                   <form id="signInForm" method="POST" action="{{loginActionUrl}}">
                       
                           <label for="username">Username *</label>
                           <input type="text" name="username" class="form-control" placeholder="Enter your username"autofocus="autofocus" required="required" />
                       
                       
                           <label for="password">Password *</label>
                           <input type="password" name="password" class="form-control" autocomplete="off" placeholder="Enter your password" required="required" />
                       
                       {{#if sessionDataKey}}
                           <input type="hidden" name="sessionDataKey" value="{{sessionDataKey}}" />
                       {{/if}}
                       {{#if referer}}
                           <input type="hidden" name="referer" value="{{referer}}" />
                       {{/if}}
                       
                           <button class="wr-btn btn-download-agent">
                               Log in
                           </button>
                           
                               <a href="{{@app.context}}/register" class="pull-right create-account">Create an account</a>
                           
                       
                   </form>
                   <h4>Other login options:</h4>
                   <a onclick="javascript: handleNoDomain('wso2.org%2Fproducts%2Fiot', 'OpenIDConnectAuthenticator')" href="#" id="icon-2">
                       <img class="idp-image" src="{{@page.publicUri}}/images/openid.png" data-toggle="tooltip" data-placement="top" title="OpenID Connect" /> Sign in with OpenID Connect
                   </a>
          {% endraw %}     
           
      {% raw %} 
    {{/zone}}
    {{~#zone "bottomJs"}}
       <script type="text/javascript">
         function handleNoDomain(key, value) {
             document.location = "../commonauth?idp=" + key + "&authenticator=" + value +
                     "&sessionDataKey={{sessionDataKey}}";
         }
       </script>
    {{/zone}}
{% endraw %} 
    

    

    

3.  Create a new file named `sign-in.json` in the `<IOTS_HOME>/` `repository/deployment/server/jaggeryapps/devicemgt/app/pages/company.page.sign-in` directory.
4.  Copy the configurations given below to the `sign-in.json` file you created.

    

    

    

    

    **sign-in.json**

    {
      "version": "1.0.0",
      "layout": "uuf.layout.sign-in",
      "uri": "/login",
      "extends": "cdmf.page.sign-in",
      "isAnonymous": true
    }

    

    

    

5.  Create a directory named `public` inside the `company.page.sign-in` directory.

6.  Create a directory named `images` inside the `company.page.sign-in/public` directory.

7.  Download the [http://demo.identityserver.io/icon.png](http://demo.identityserver.io/icon.png) file and rename the downloaded file to `openid.png`.

8.  Copy the `openid.png` file to the `company.page.sign-in/public/images` directory.

Now, you have configured Entgra IoT Server successfully. Let's try it out and see.

### Try it out

Follow the steps given below:

1.  Start the Entgra IoT Server's core profile.

    cd <IOTS_HOME>/bin
    ./iot-server.sh

2.  Access the device management console: `https://<IOTS_HOST>:<IOTS_HTTPS_PORT>/devicemgt`

    

    

    *   By default, `<IOTS_HOST>` is `localhost.` However, if you are using a public IP, the respective IP address or domain needs to be specified.
    *   By default, `<IOTS_HTTPS_PORT>` is set to 9443\. However, if the port offset is incremented by `n`, the default port value needs to be incremented by `n`.

    

    

3.  Click **Sign in with OpenID Connect**.  
    ![image](352822910.png)  
    You are redirected to the OpenID connect log in page.
4.  Enter bob as the username and bob as the password, and click **Login**. This is the default username and password for the IdentityServer4 demo site.  
    ![image](352822916.png)  
    Once the authentication is successful, you are redirected to the device management console. 

**Congratulations!** You have successfully configured setting up federated IdP with OpenID Connect for Entgra IoT Server.





When starting off the user bob did not exist in Entgra IoT Server. Because you configured JIT provisioning for the IdP, you see that a new user is created under **Users** in the Entgra IoT Server device management console. The new users name is denoted by as a string of integers, such as 88421113.

Follow the steps given below to check the role of the user that got created:

1.  [Access the IoT Server management console](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console).

2.  On the **Main** tab in the Management Console, click **List** under **Users and Roles**.
3.  Click **Users**. This link is only visible to users with the Admin role.
4.  Click **View Roles** next to the new user.  
    ![image](352822904.png)  
    The `Internal/devicemgt-admin` role is assigned because you configured Entgra IoT Server to assign this role to all the JIT provisioned users in [step 6 when configuring the Entgra IoT Server IdP](http://localhost:1313/v3.8.0/docs/Document/using-entgra-iot-server/entgra-iot-server-security/setting-up-a-federated-idps/#configuring-the-entgra-iot-server-idp).
