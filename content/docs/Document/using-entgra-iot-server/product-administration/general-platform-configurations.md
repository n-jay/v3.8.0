# General Platform COnfigurations

Configure how often the devices enrolled with Entgra IoT Server need to be monitored via the general platform configurations and deploy the geo analytics artifacts required for location based services in a multi tenant environment using the general platform configurations.

Follow the instructions below to configure the general platform settings:

1.  [Sign in to the Entgra IoTS device management console](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/) and click the menu icon.  

    ![image](352821220.png)
2.  Click **Platform Configurations**.  
    ![image](352821214.png)
3.  Define the **Monitoring Frequency** in seconds, to monitor the enforced policies on the devices, and click  **SAVE**.  
    ![image](352821230.png)
4.  The geo analytics artifacts are deployed by default in the Entgra IoT Server super tenant. However, if you are setting up geofencing or location based services in a multi-tenant environment, you have to deploy the geo analytics artifacts in each tenant. 

    

    

    For more information, see [Monitoring Devices Using Location Based Services](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Entgra-IoT-Server-Analytics/Monitoring-Devices-Using-Location-Based-Services/).


    

    

    1.  Log in to the device management console using the tenant credentials.

    2.  Click on ![image](2.png), and select** Configuration Management > Platform Configurations**.

    3.  Click the **Deploy Geo Analytics Artifacts** button. You can also use this button to re-deploy the geo analytics artifacts in super tenant mode if required.   
        ![image](352821235.png)
