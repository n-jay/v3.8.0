# Clusturing Entgra IoT server

# Tanenet Management

The goal of multi-tenancy is to maximize resource sharing across multiple users (while hiding the fact that these users are on the same server) and to ensure optimal performance. You can register tenants in the Management Console, allowing tenants to maintain separate domains for their institutions.





When multi-tenancy is used, certain tenants can sometimes become inactive for a long period of time. By default, if the inactive period is 30 minutes, the tenant is unloaded from the server memory. This requires the tenant to log in again before sending requests to the server.

If required, you can change the default time allowed for tenant inactiveness by adding  the `-Dtenant.idle.time=<time_in_minutes>` java property to the product startup script ( `./iot-server.sh` file for Linux and   `iot-server.bat`  for Windows) as shown below:


JAVA_OPTS \
    -Dtenant.idle.time=30 \






All the Entgra IoTS tenants can access the public Store, but tenants are always required to log in to view their private store. Before carrying out any tasks on the Management Console, we highly recommend you to change the default super tenant administrator password. You can also create a new tenant.

## Changing the default tenant password

Before carrying out any tasks on the Management Console, we highly recommend you to change the default super tenant administrator password. Follow the instructions below to change the default super tenant administrators password:

1.  Open a command prompt:

*   On Windows, choose Start -> Run, type cmd at the prompt, and press Enter.
*   On Linux/Solaris, establish a SSH connection to the server or log in to the text Linux Console.

3.  Execute one of the following commands, where `<IOTS_HOME>` is the directory where you installed the product distribution:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>OS</th>
          <th>Command</th>
        </tr>
        <tr>
          <td>Linux/Solaris</td>
          <td>
            
          </td>
        </tr>
        <tr>
          <td>Windows</td>
          <td>
            
          </td>
        </tr>
      </tbody>
    </table>

4.  Log into the Management Console using the following URL format:   
    `https://<IOTS_HOST>:<IOTS_HTTPS_PORT>/admin/carbon`

    *   By default, `<IOTS_HOST>` is `localhost.` However, if you are using a public IP, the respective IP address or domain needs to be specified.
    *   By default,  `<IOTS_HTTPS_PORT>` has been set to 9443\. However, if the port offset has been incremented by `n`, the default port value needs to be incremented by `n`.

    For example:  
    `http://localhost:9443/admin/carbon/`

5.  Enter the username as `admin` and the password as `admin`.
6.  Click **Sign-in**.
7.  Switch to the **Configure** tab.  
    ![image](352819995.png)
8.  Click **Users and Roles** and thereafter click **User**.
9.  Click **Change Password** in the Actions column, in-line with the user `admin` and set a new password.
10.  Click** Change**.
11.  Log out and log in again with the new credentials.

## Adding a new tenant

You can add a new tenant in the management console and then view it by following the procedure below. In order to add a new tenant, you should be logged in as a super user.

1.  Click **Add New Tenant** in the **Configure** tab of the IoTS management console.  
    ![image](352819989.png)
2.  Enter the tenant information in **Register A New Organization** screen as follows, and click **Save**.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Parameter Name</th>
          <th>Description</th>
        </tr>
        <tr>
          <td><strong>Domain</strong></td>
          <td>The domain name for the organization, which should be unique (e.g., abc.com).&nbsp;This is used as a unique identifier for your domain. You can use it to log into the admin console to be redirected to your specific tenant. The domain is also used in URLs to distinguish one tenant from another.&nbsp;</td>
        </tr>
        <tr>
          <td><strong>Select Usage Plan for Tenant</strong></td>
          <td>The usage plan defines limitations (such as number of users, bandwidth etc.) for the tenant.</td>
        </tr>
        <tr>
          <td><strong>First Name</strong>/<strong>Last Name</strong></td>
          <td>The name of the tenant admin.</td>
        </tr>
        <tr>
          <td><strong>Admin Username</strong></td>
          <td>&nbsp;The login username of the tenant admin. The username always ends with the domain name (e.g.,<a rel="nofollow">&nbsp;admin@abc.com</a>)</td>
        </tr>
        <tr>
          <td><strong>Admin Password</strong></td>
          <td>The password used to log in using the admin username specified.</td>
        </tr>
        <tr>
          <td><strong>Admin Password (Repeat)</strong></td>
          <td>Repeat the password to confirm.</td>
        </tr>
        <tr>
          <td><strong>Email</strong></td>
          <td>The email address of the admin.</td>
        </tr>
      </tbody>
    </table>

3.  After saving, the newly added tenant appears in the **Tenants List** page as shown below. Click** View Tenants** in the **Configure** tab of the management console to see information of all the tenants that currently exist in the system. If you want to view only tenants of a specific domain, enter the domain name in the **Enter the Tenant Domain** parameter and click **Find**.  
    ![image](352819983.png)

## Editing a tenant

Follow the instructions below to edit a tenant:

1.  Log into the Management Console.
2.  Switch to the **Configure** tab and click** View Tenants**.
3.  Search for the tenant based on the tenant domain name.
4.  Click **Edit**.
5.  Edit the tenant details as required. Note that the domain, tenant ID and usage plan can not be modified.
6.  Click Update.
7.  Optionally, click **Deactivate,** if you wish to deactivate the tenant.


# Customizing email templates for tanants


If you wish to update the email template for a tenant, you can do so by following the steps given below:

**Prerequisite**



Add a new tenant. For more information, see [adding a new tenant](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/tenant-management/).   
Example: The added tenant has the user name `admin@abc.com`.



1.  [Start the server](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/).


2.  Access the IoTS Management Console using one of the following URLs:

    *   Accessing the console via HTTP:``http://<HTTP_HOST>:<HTTP_PORT>/carbon/`` Example:` http://localhost:9763/carbon/`
    *   Accessing the console via HTTPS:   
        `https://<HTTPS_HOST>:<HTTPS_PORT>/carbon/` 
        Example:` https://localhost:9443/carbon/`![image](9443.png)
3.  Enter the username and password of the tenant you created and click **Sign-in**.  
    Example: username: admin@abc.com and password: tenant123$.
4.  Click **Browse** in the **Main** tab of the WSO2 IoTS management console.
5.  Navigate to email-templates by entering `/_system/config/email-templates` in the **Location** field, and clicking **GO**.  
    ![image](352820048.png)
6.  Click the template you want to update.  
    Example: Click user-enrollment.  
    ![image](352820015.png)
7.  Click **Edit as text**.  
    ![image](352820021.png)
8.  Select **Plain Text Editor** to edit the template style sheet.  
    ![image](352820037.png)
9.  Click **Save** to save the updated template.



# Sharing iOS Platform Configurations Among Tenants

Want to share the super tenant iOS platform configurations with your tenants too? This document guides you to share the iOS platform configurations among tenants.



Note



This approach is **not recommended for your production environment**. If you want to enable multi-tenancy for iOS platform configurations in a production environment, you need to configure the platform configuration for each tenant via the device management console. For more information, see [iOS Platform Configurations](https://entgra-documentation.gitlab.io/v3.7.0/docs/Working-with-Android-Devices/Working-with-Android-Devices.html).








Before you begin!



Configure the iOS platform configurations of the super tenant. For more information, see [iOS Platform Configurations](https://entgra-documentation.gitlab.io/v3.7.0/docs/Working-with-Android-Devices/Working-with-Android-Devices.html). The default super tenant username is `admin` and the password is `admin`.






Follow the steps given below:

1.  Copy the iOS platform configurations of the super tenant:
    1.  Log in to the IoT server's core carbon console: `https://<IOTS_HOST>:9443/carbon`

        

        

        The default HTTPS port of the core profile is 9443\. If you port offset WSO2 IoT Server, make sure to use the correct port to access the carbon console.

        

        

    2.  Click **Main > Resources > Browse**.
    3.  Enter `/_system/config/ios` as the **Location** and click **Display as text**.  
        ![image](352820095.png)
    4.  Copy the content that appears in the text area.  
        ![image](352820101.png)
2.  Create an XML file named `ios-default-platform-configuration.xml` in the `<IOTS_HOME>/conf/etc/device-mgt-plugin-configs/mobile` directory.
3.  Paste the content you copied in step 1 to the `ios-default-platform-configuration.xml` file you created in step 2.
4.  Restart WSO2 IoT Server.
