# iOS Configurations


This section guides you on how to configure Entgra IoT Server with the iOS settings. It must be noted that this process requires many configurations, therefore you must follow all the steps carefully.



Prerequisites



*   If you need to build the iOS client application (iOS Agent), then the following is required.

    

    

    For more information, see the frequently asked questions section to know [why you need to use the iOS Agent](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824866/FAQ#FAQ-WhydoIneedtousetheiOSAgent?).

    

    

    *   You have to be enrolled in the [Apple Developer Program](https://developer.apple.com/programs/) as an individual or organization before starting the iOS server configurations.

    *   Download and install [Xcode](https://developer.apple.com/xcode/download/).
*   Download and install OpenSSL. For more information, see how to [download and install OpenSSL](https://www.openssl.org/source/). 





Before you begin, let's take a look at the diagram given below to get a snapshot view of the steps you need to follow:   

![image](352823930.png)

Follow the steps given below in the given order:

## Step 1: Obtain the iOS features





Follow the steps given below to get a signed CSR file and P2 repository.

1.  **Fill in the following [iOS form](http://wso2.com/products/iot-server/csr/) via the WSO2 site.**  
    WSO2 will send you an email with the following:

    *   The P2 repository with the iOS features, End User License Agreement (EULA) and the README.txt. 

    *   The iOS agent source code. You can use this if you want to customize the iOS agent application in Entgra IoT Server.

2.  Click [here to access the iOS agent source code](https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.1).
3.  **Get your certificate signed by Apple**  
    Register your organization with the [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/). Thereafter, follow the steps mentioned in [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html). 





## Step 2: iOS server configurations





For more information, see [iOS Server Configurations](/doc/en/lb2/iOS-Server-Configurations.html).





## Step 3: Generate the certificate from the Apple Developer Portal





For more information, see [Generating Certificates from the Apple Developer Portal](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/working-with-ios/ios-configurations/#generating-certificates-from-the-apple-developer-portal).





## Step 4: iOS client configurations





For more information, see [iOS Client Configurations](/doc/en/lb2/iOS-Client-Configurations.html). This step is not required if you are not using the Entgra IoT Server iOS agent.





## Step 5: iOS platform configurations





For more information, see [iOS Platform Configurations](/doc/en/lb2/iOS-Platform-Configurations.html).

# Obtaining the iOS Features


To test out the iOS device enrollment process of Entgra IoT Server, you need to install the iOS features to the Entgra IoT Server pack and have the required certificates made available via Apple. 

1.  Fill the [iOS form](http://wso2.com/products/iot-server/csr/) on the WSO2 site to request for the P2 repository, which contains the iOS feature deployer.   
    This is needed to configure Entgra IoT Server for iOS. 
2.  Your request will be reviewed and our Account Managers will reach out to you with the following: 
    *   The P2 repo that has the iOS feature deployer.
    *   The agent source code (you can use this if you want to customize the iOS agent application).
3.  Click [here to access the iOS agent source code](https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.1).
4.  Register your organization with the [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/). Then, follow the steps mentioned in [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html)

# Generating Certificates from the Apple Developer Portal


Follow the subsections given below to generate the Apple Push Notification Service (APNS) certificate and the MDM APNS certificate via the Apple Developer Program.

*   [Generating an MDM APNS Certificate](/doc/en/lb2/Generating-an-MDM-APNS-Certificate.html)
*   [Generating an APNS Certificate](/doc/en/lb2/Generating-an-APNS-Certificate.html)


## Generating an MDM APNS Certificate


Follow the instructions below to generate the MDM Apple Push Notification Service (APNS) certificate:



Why is this step required?



This certificate is required to carry out operations on the device that need to be triggered via the iOS operating system (OS). Therefore, this certificate is mandatory to enroll your iOS device with Entgra IoT Server.

Further, in iOS, the server passes messages to the client via the [Apple Push Notification Service (APNS)](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/working-with-ios/ios-notification-method/). When doing so in order to establish a secure connection between Entgra IoT Server and the APNS server, a client SSL certificate needs to be generated and downloaded from Apple Inc. This APNS certificate is used to send an awake message to your iOS device.









The MDM APNS certificate will be referred to as the MDM certificate in the WSO2 device management console.





1.  Go to the Apple Push Certificate Portal at **[https://identity.apple.com/pushcert/](https://identity.apple.com/pushcert/)** and log in with your customer account details.

    

    

    You do not need to have an enterprise account for this purpose, all you need is your Apple ID. If you don't have one, create **[your Apple ID](https://appleid.apple.com/account#!&page=create)**.

    

    

    1.  Click **Create Certificate** and agree to the terms and conditions.
    2.  Upload the encoded `.plist` file you generated by following the steps given under [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html). 
    3.  Download the generated MDM signing certificate (`MDM_Certificate.pem`). The MDM signing certificate is a certificate for 3rd party servers provided by Apple.
2.  Note down the `USERID` (TOPIC ID) from the MDM signing certificate (`MDM_Certificate.pem)` as it will be used later in the configuration. The MDM signing certificate can be decoded to obtain the `USERID` by executing the following command:

    `openssl x509 -in MDM_Certificate.pem -text -noout`

3.  Remove the password from your private key file (e.g., `customerPrivateKey.pem`).

    `openssl rsa -in customerPrivateKey.pem -out customerKey.pem `

4.  Merge the customer key file that was derived in the latter step, with the MDM signing certificate to generate the MDM Apple Push Notification Service (APNS) Certificate.  
    For example, merge the `customerKey.pem` file with the `MDM_Certificate.pem` file to generate the `MDM_APNSCert.pem` file.

    `cat MDM_Certificate.pem customerKey.pem > MDM_APNSCert.pem`

5.  Open the MDM Apple Push Notification service (APNs) Certificate (`MDM_APNSCert.pem`) and ensure that there is a line break between the contents of the two files.  
    Example:  
    The content will look as follows:`-----END CERTIFICATE----------BEGIN RSA PRIVATE KEY-----`  
    Therefore, add a line break to separate the 2 certificates **after 5 `-` (dashes)** so that the content will look like what's shown below:

    
    -----END CERTIFICATE-----
    -----BEGIN RSA PRIVATE KEY-----

6.  Convert the `MDM_APNSCert.pem` file to the `MDM_APNSCert.pfx` file. You will need to provide a password when converting the file. Thereafter, follow the steps mentioned under [iOS Platform Configurations](/doc/en/lb2/iOS-Platform-Configurations.html).

    `openssl pkcs12 -export -out MDM_APNSCert.pfx -inkey customerPrivateKey.pem -in MDM_APNSCert.pem`

## Generating an APNS Certificate 

This section guides you on how to generate an APNS certificate.



Why is this step required?



You can register an iOS device with Entgra IoT Server, with or without the Entgra IoT Server's iOS agent. This certificate is required to carry out operations on the device that need to be triggered via the iOS agent, such as ringing the device, getting the device location, and sending notifications or messages to the device. Therefore, if you are not installing the iOS agent on your devices, you don't need this certificate.

Further, in iOS, the server passes messages to the client via the [Apple Push Notification 
Service (APNS)](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/working-with-ios/ios-notification-method/). When doing so in order to establish a secure connection between Entgra IoT Server and the APNS server, a client SSL certificate needs to be generated and downloaded from Apple Inc. This APNS certificate is used to send an awake message to the iOS agent application.







Prerequisites



*   You have to be enrolled in the  [Apple Developer Program](https://developer.apple.com/programs/) as an individual or organization before starting the iOS server configurations.
*   A valid distribution certificates that you obtained from Apple.





Follow the steps given below:

1.  Clone the [`emm-agent-ios`](https://github.com/wso2/emm-agent-ios) repository to a preferred location.

    `git clone https://github.com/wso2/emm-agent-ios`

2.  Open the `emm-agent-ios` from X-Code and follow the subsequent steps:
    1.  Change the `org.wso2.carbon.emm.ios.agent` Bundle Identifier so that it matches your organization details.  
        Example:`org.<ORGANIZATION_NAME>.emm.ios.agent`
    2.  Select the development team, provisioning profile and sign certificate from Xcode.

        

        

        If you are unsure of how to select the development team, or add the provisioning profile or sign the certificate via Xcode, see the blog post on [How to export “in-house” developed iOS app as an enterprise application](https://medium.com/@mharindu/how-to-export-in-house-developed-ios-app-as-an-enterprise-application-dc087bdd64c3#.ptkvra6o3).

        

        

3.  [Log in to the Apple Developer ](https://idmsa.apple.com/IDMSWebAuth/login?appIdKey=891bd3417a7776362562d2197f89480a8547b108fd934911bcbea0110d07f757&path=%2Faccount%2F&rv=1)program and follow the subsequent steps:

    

    

    Before you follow the steps, confirm that your machine is connected to the Internet and that Xcode has a valid developer account.

    

    

    1.  Navigate to **Certificates, IDs & Profiles** that is under **Identifiers**.
    2.  Click **App IDs** and see if the Bundle ID that you defined under Xcode is listed here.  
        ![image](352824045.png)
4.  Click the Bundle ID, and click **Edit**.  
    ![image](352824039.png)
5.  Creating an APNs SSL certificate:
    1.  Select Push Notifications to enable the setting.  
        ![image](352824016.png)  
        Once push notification is enabled, you are able to generate the development and production certificates.
    2.  To try out the create certificate use case, let's create a development SSL certificate.  
        Please note that the development SSL certificate is created only as an example. You can create a production SSL certificate if you have registered with the Apple Developer Program as an Organization.  

        Click **Create Certificate** that is under Development SSL Certificate.  
        ![image](352824010.png)
6.  Creating a CSR file using the keychain access tool in the Mac OS:
    1.  Launch the keychain access application.
    2.  On the menu bar click **KeyChain Access > Certificate Assistant > Request a Certificate from Certificate Authority**.  
        ![image](352824028.png)
    3.  Define the email address, common name, select **Saved to disk**, and click Continue.  
        Example:  
        ![image](352824022.png)
7.  Go back to the Apple Developer Portal, upload the generated certificate, and click **Continue**.  
    ![](352824004.png)
8.  Exporting the certificate to the `pfx` format.
    1.  Click **Download** to download the file.  
        ![image](352823998.png)
    2.  Double-click the downloaded file to open it with the Keychain access tool.
    3.  Right-click the certificate and select export.  

    4.  Define the location where you wish to save the file and set a password for the exported file when prompted.
    5.  Rename the `p12` extensionof the file to `pfx`.


# iOS Server Configurations

Follow the instructions below to configure the iOS server-side configurations:

## **Step 1 - Install Entgra IoTS iOS features via the P2 repository**





For more information on installing the P2 repository, see [Installing iOS Features](/doc/en/lb2/Installing-iOS-Features.html).





## **Step 2 - Configure the IP for iOS**





For more information, see [Configuring Entgra IoT Server with the IP or Hostname for iOS](/doc/en/lb2/Configuring-Entgra-IoT-Server-with-the-IP-or-Hostname-for-iOS.html).





**Step 3 - Configuring Entgra IoT Server to install applications**





For more information, see [Configuring Entgra IoT Server to Install iOS Applications](/doc/en/lb2/Configuring-Entgra-IoT-Server-to-Install-iOS-Applications.html).





**Step 4 - Configure the general iOS server settings**





This section includes the details on how to generate the Certificate Authority (CA), Registration Authority (RA) and SSL certificate.

For more information on general iOS server configurations, see [General iOS Server Configurations](/doc/en/lb2/General-iOS-Server-Configurations.html).



## Installing iOS Features

The P2 repository contains the feature list required to enable iOS.  





If you are on a Windows OS, be sure to point the `-Dcarbon.home` property in the product's startup script (`wso2server.bat`) to the product's distribution home (e.g., `-Dcarbon.home=C:\Users\VM\Desktop\wso2iot-3.0.0`). Alternatively, you can also set the `carbon.home` as a system property in Windows. Then, restart the server. Without this setting, you might not be able to install features through the management console.







Where do I get the P2 repository from?



 For more information on obtaining the P2 repository, see [Obtaining the iOS Features](/doc/en/lb2/Obtaining-the-iOS-Features.html).





Follow the instructions given below to install the iOS features via the iOS feature deployer  you received via email:

1.  Download and extract the `ios-feature-deployer`.

2.  Copy the ios-feature-deployer folder to the `<IOTS_HOME>` directory.  
3.  Navigate inside the ios-feature-deployer folder on the terminal and execute the following command to install the iOS features to Entgra IoT. 

    cd ios-feature-deployer 
    mvn clean install -f ios-feature-deployer.xml

    

    Tip by Chris

    

    To verify successful installation of the iOS features, do the following:

    Open the [`bundles.info`](http://bundles.info/) file found in the `<IOTS-HOME>/wso2/components/default/configuration/org.eclipse.equinox.simpleconfigurator` directory and verify if the following properties have changed from `false` to **`true`**.

    org.wso2.carbon.device.mgt.ios.api.utils,3.0.5,../plugins/org.wso2.carbon.device.mgt.ios.api.utils_3.0.5.jar,4,true

    org.wso2.carbon.device.mgt.ios.apns,3.0.5,../plugins/org.wso2.carbon.device.mgt.ios.apns_3.0.5.jar,4,true

    org.wso2.carbon.device.mgt.ios.core,3.0.5,../plugins/org.wso2.carbon.device.mgt.ios.core_3.0.5.jar,4,true

    org.wso2.carbon.device.mgt.ios.payload,3.0.5,../plugins/org.wso2.carbon.device.mgt.ios.payload_3.0.5.jar,4,true

    org.wso2.carbon.device.mgt.ios.plugin,3.0.5,../plugins/org.wso2.carbon.device.mgt.ios.plugin_3.0.5.jar,4,true

    

    

4.  Uncomment the `APNSBasedPushNotificationProvider` that is under the `PushNotificationProviders` configuration in the `<IOTS_HOME>/conf/cdm-config.xml` file.

    <PushNotificationProviders>    		   
        <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.mqtt.MQTTBasedPushNotificationProvider</Provider>
        <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.xmpp.XMPPBasedPushNotificationProvider</Provider>
        <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.gcm.GCMBasedPushNotificationProvider</Provider>
        <Provider>org.wso2.carbon.device.mgt.mobile.impl.ios.apns.APNSBasedPushNotificationProvider</Provider>
    </PushNotificationProviders>



## Configuring Entgra IoT Server with the IP or Hostname for iOS

If you are in a **production environment**, make sure to have the following ports open:

*   5223 - TCP port used by devices to communicate to APNs servers
*   2195 - TCP port used to send notifications to APNs
*   2196 - TCP port  used by the APNs feedback service
*   443 - TCP port used as a fallback on Wi-Fi, only when devices are unable to communicate to APNs on port 5223  
    The APNs servers use load balancing. The devices will not always connect to the same public IP address for notifications. The entire 17.0.0.0/8 address block is assigned to Apple, so it is best to allow this range in the firewall settings. 
*   10397 - Thrift client and server ports
*   8280, 8243 - NIO/PT transport ports





Entgra IoT Server is configured via localhost as the product has SSO enabled by default. However, when configuring Entgra IoT Server with iOS, you need to make it IP or hostname based instead of localhost so that the iOS agent can communicate with the Server. Follow the steps given below to configure the IP or hostname in Entgra IoT Server. 

### Configuring the IP using the script

This section provides a script that automatically configures the IP address when executed. This method is recommended because manually configuring the IP address includes many steps which may cause errors if not followed carefully. 

1.  Thie script automatically configures the IP and creates the required SSL certificates for the IP or hostname. This method is recommended because manually configuring the IP address includes many steps and if you miss out on a step you will run into errors.

    

    

    If you want to configure the steps manually, see [Configuring the IP or hostname manually](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/configuring-ip-or-hostname/) and if you want to change the default ports, see [Changing the Default Ports](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/product-administration/changing-the-default-ports/#changing-the-default-ports).


    

    

    1.  Navigate to the `<IOTS_HOME>/scripts` directory.
    2.  Run the `change-ip` script.

        

        

        **Tip:** The script will find and replace the IP address given in argument1 (`localhost`) with the IP address given as argument2 (`10.10.10.14`), in the necessary configuration files. 

        

        

    1.  Change the current IP address of the IoT Server core, broker, and analytics profile.

    2.  Enter the values for IoT Server core SSL certificate.

        

        

        

        Enter the requested information when prompted.

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <thead>
            <tr>
              <th>
                
              </th>
              <th>
                
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Country</td>
              <td>The name of your country. Enter the two digit code for your country.</td>
            </tr>
            <tr>
              <td>State</td>
              <td>The state your organization is at.</td>
            </tr>
            <tr>
              <td>Location</td>
              <td>The city your organization is located at.</td>
            </tr>
            <tr>
              <td>
                <p>Organization</p>
              </td>
              <td>
                <p>The name of your organization. For this scenario, we entered wso2.</p>
              </td>
            </tr>
            <tr>
              <td>Organization Unit</td>
              <td>Defined the Team ID as the organization unit.</td>
            </tr>
            <tr>
              <td>
                <p>Email</p>
              </td>
              <td>
                <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a href="mailto:chris@wso2.com" class="external-link" rel="nofollow">chris@wso2.com</a>&nbsp;as the email.</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Commonname</p>
              </td>
              <td>
                <p>Fully qualified domain name of your server.</p>
              </td>
            </tr>
          </tbody>
        </table>

        ![image](352824203.png)

        

        

2.  Navigate to the `<IOTS_HOME>/ios-configurator` directory.

    

    

    You will not have this directory if you did not follow the steps given in [Installing iOS Features](/doc/en/lb2/Installing-iOS-Features.html).

    

    

3.  To configure Entgra IoT Server with the IP, run the `ios.sh` script with the IP addresses as arguments. 

    

    

    This part of the script creates a key pair, generates a signature, and signs the key using the signature. Next, you will be prompted for an IP address. 

    

    

    

    

    The script will find and replace the IP address given in argument1 (`localhost`) with the IP address given as argument2 (`10.10.10.14`), in the necessary configuration files.

    

    

    `./ios.sh`

### Configuring the IP manually

This section provides detailed steps on how to configure the IP address manually (as an alternative to using the script given above). 

1.  Configure Entgra IoT Server with the IP:

    1.  Open the `<IOTS_HOME>/conf/carbon.xml` file and configure the `<HostName>` and `<MgtHostName>` attributes with the {`IoT_SERVER_HOSTNAME}`.

        <HostName>{IoT_SERVER_IP/HOSTNAME}</HostName>
        <MgtHostName>{IoT_SERVER_IP/HOSTNAME}</MgtHostName>

    2.  Open the `<IOTS_HOME>/conf/identity/sso-idp-config.xml` file, and find and replace `localhost` with the `<IoT_SERVER_IP/HOSTNAME` `>`.

    3.  Open the `<IOTS_HOME>/conf/api-manager.xml` file and configure the `<DASServerURL>` attribute by replacing localhost with the IoT Server IP or hostname.

        `<DASServerURL>{tcp://<IoT_SERVER_IP/HOSTNAME>t:7613}</DASServerURL>`

    4.  Open the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file, and set `true` as the value for `<EnabledUpdateApi>`.

        <!-- If it is true, the APIs of this instance will be updated when the webapps are redeployed -->
        <EnabledUpdateApi>true</EnabledUpdateApi>

        

        

        If you have not started Entgra IoT Server previously, you don't need this configuration. When the server starts for the first time it will update the APIs and web apps with the new server IP.

        

        

        

        

        Make sure to configure this property back to `false` if you need to restart the server again after the configuring the IP.  

        By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.

        

        

    5.  Open the `<IOT_HOME>/repository/deployment/server/jaggeryapps/api-store/site/conf/site.json` file, and configure the `identityProviderUrl` attribute by replacing localhost with the IoT Server IP or hostname.

        `"identityProviderURL" : "https://<IoT_SERVER_IP/HOSTNAME>:9443/samlsso",`

    6.  Open the `<IOT_HOME>/wso2/analytics/repository/deployment/server/jaggeryapps/portal/configs/designer.json` file, and configure the `identityProviderUrl`, `acs`, and `host` attributes by replacing `localhost` with the IoT Server IP or hostname and the respective profiles port.

        "identityProviderURL": "https://<IoT_SERVER_IP/HOSTNAME>:9443/samlsso",
        "acs": "https://<IoT_SERVER_IP/HOSTNAME>:9445/portal/acs",
        "host": {
          "hostname": "<IoT_SERVER_IP/HOSTNAME>",
          "port": "",
          "protocol": ""
        },

        

        

        The default port of the Entgra IoT Server profiles are as follows:

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>Entgra IoT Server core profile</th>
              <td>9443</td>
            </tr>
            <tr>
              <th>Entgra IoT Server analytics profile</th>
              <td>9445</td>
            </tr>
            <tr>
              <th>Entgra IoT Server broker profile</th>
              <td>9446</td>
            </tr>
          </tbody>
        </table>

        Therefore, the analytics portal needs to be assigned the 9445 port.

        

        

    7.  Open the `<IOTS_HOME>/bin/iot-server.sh` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`. If you are running on Windows, you need to configure the `iot-server.bat` file.

        -Diot.analytics.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.manager.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Dmqtt.broker.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.core.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.keymanager.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.gateway.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.apimpublisher.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.apimstore.host="<IoT_SERVER_IP/HOSTNAME>" \

    8.  Open the `<IOTS_HOME>/wso2/analytics/bin/wso2.server.sh` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`. If you are running on Windows, you need to configure the `wso2server.bat` file.

        -Dmqtt.broker.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.keymanager.host="<IoT_SERVER_IP/HOSTNAME>" \
        -Diot.gateway.host="<IoT_SERVER_IP/HOSTNAME>" \

    9.  Open the `<IOTS_HOME>/wso2/broker/conf/broker.xml` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`:

        <authenticator class="org.wso2.carbon.andes.authentication.andes.OAuth2BasedMQTTAuthenticator">
           <property name="hostURL">https://<IoT_SERVER_IP/HOSTNAME>:9443/services/OAuth2TokenValidationService</property>
           <property name="username">admin</property>
           <property name="password">admin</property>
           <property name="maxConnectionsPerHost">10</property>
           <property name="maxTotalConnections">150</property>
        </authenticator>

        <authorizer class="org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization.DeviceAccessBasedMQTTAuthorizer">
           <property name="username">admin</property>
           <property name="password">admin</property>
           <property name="tokenEndpoint">https://<IoT_SERVER_IP/HOSTNAME>t:8243</property>
           <!--offset time from expiry time to trigger refresh call - seconds -->
           <property name="tokenRefreshTimeOffset">100</property>
           <property name="deviceMgtServerUrl">https://<IoT_SERVER_IP/HOSTNAME>t:8243</property>
        </authorizer>

    10.  Optionally, if you are using the [Entgra Android auto-enrollment feature](https://entgra-documentation.gitlab.io/v3.7.0/docs/Working-with-Android-Devices/Working-with-Android-Devices.html), you need to replace all the `localhost` references to the IP or hostname in the following files that are in the


        `<IOTS_HOME>/repository/deployment/server/synapse-configs/default/api` directory.
        *   `admin--Android-Mutual-SSL-Event-Receiver.xml`
        *   `admin--Android-Mutual-SSL-Device-Management.xml`
        *   `admin--Android-Mutual-SSL-Configuration-Management.xml`
    11.  If you are using the hostname instead of the IP, open the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/conf/config.json` file and configure the `androidAgentDownloadURL` property.

        `"androidAgentDownloadURL": "https://%iot.manager.host%:%iot.manager.https.port%/android-web-agent/public/mdm.page.enrollments.android.download-agent/asset/android-agent.apk",`

    12.  Run the following commands so that the self-signed certificate refers to the IP you just configured instead of `localhost`.

        

        

        This step is required if your devices are accessing Entgra IoT Server from outside the server.

        

        

        

        

        Because of the changes made to the keystore, you will not able to access the tenants that are already created in Entgra IoT Server. Therefore, **it is recommended to keep a backup of the tenants** when changing the IP or hostname.

        

        

        1.  Navigate to the `<IOTS_HOME>/repository/resources/security` directory and run the following commands to create the `client-truststore.jks` and `wso2carbon.jks` files with the new IP or hostname. If ask for key

            keytool -delete -alias wso2carbon -keystore wso2carbon.jks -storepass wso2carbon -keypass wso2carbon

            keytool -genkey -alias wso2carbon -keyalg RSA -keysize 2048 -keystore wso2carbon.jks -dname "CN=<IOT_SERVER_IP/HOSTNAME>,
            OU=Home,O=Home,L=SL,S=WS,C=LK" -storepass wso2carbon -keypass wso2carbon

            keytool -delete -alias wso2carbon -keystore client-truststore.jks -storepass wso2carbon -keypass wso2carbon

            keytool -export -alias wso2carbon -keystore wso2carbon.jks -file wso2carbon.pem -storepass wso2carbon -keypass wso2carbon

            keytool -import -alias wso2carbon -file wso2carbon.pem -keystore client-truststore.jks -storepass wso2carbon

        2.  Update the Identity Provider (IDP) with the new certificate:

            1.  Export wso2carbon.pem certificate that is in the binary DER format to the ASCII PEM format.

                `openssl x509 -inform DER -outform PEM -in wso2carbon.pem -out server.crt`

            2.  Open the `server.crt` file you just generated and copy the content that is between the `BEGIN CERTIFICATE` and `END CERTIFICATE`.

                

                

                Make sure to remove the new lines that are there in the certificate. Else, the JWT validation fails.

                

                

            3.  Open the `<IOTS_HOME>/conf/identity/identity-providers/iot_default.xml` file and replace the content that is under the `<Certificate>` property with the content you just copied.

        3.  Copy the `client-truststore.jks` and `wso2carbon.jks` files that you created in step **13.a** to the following locations.

            

            

            Make sure to only copy the files. Don't remove it from the `<IOTS_HOME>/repository/resources/security` directory.

            

            

            *   `<IOTS_HOME>/wso2/broker/repository/resources/security`

            *   `<IOTS_HOME>/wso2/analytics/repository/resources/security`

    13.  Once you are done with the above steps, restart or start the message broker, IoT Server core, and the analytics profiles in the given order. For more information, see [Starting the Server](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/installation-guide/Running-the-Product/#starting-the-server).


2.  Update the following parameters in the `ios-config` `.xml` file, which is in the `<IoT_HOME>/conf` directory:   
    Enter the server IP or the server domain name for the following parameters:

    *   `iOSEnrollURL`

    *   `iOSProfileURL`

    *   `iOSCheckinURL`

    *   `iOSServerURL` `TokenURL`

    For example:

    <?xml version="1.0" encoding="ISO-8859-1"?>
    <iOSEMMConfigurations>
        <!-- iOS MDM endpoint urls -->
        <iOSEnrollURL>https://10.10.10.253:8243/api/ios/v1.0/scep</iOSEnrollURL>
        <iOSProfileURL>https://10.10.10.253:8243/api/ios/v1.0/profile</iOSProfileURL>
        <iOSCheckinURL>https://10.10.10.253:8243/api/ios/v1.0/checkin</iOSCheckinURL>
        <iOSServerURL>https://10.10.10.253:8243/api/ios/v1.0/server</iOSServerURL>
    </iOSEMMConfigurations>

3.  Open the `<IoTS_HOME>/conf/iot-api-config.xml` file and replace `localhost` with your IP or hostname.  
    For example: 

    <ServerConfiguration>
        <!-- IoT server host name, this is referred from APIM gateway to call to IoT server for certificate validation-->
        <Hostname>https://10.10.10.253:9443/</Hostname>
        <!--End point to verify the certificate-->
        <VerificationEndpoint>https://10.10.10.253:9443/api/certificate-mgt/v1.0/admin/certificates/verify/</VerificationEndpoint>
        <!--Admin username/password - this is to use for oauth token generation-->
        <Username>admin</Username>
        <Password>admin</Password>
        <!--Dynamic client registration endpoint-->
        <DynamicClientRegistrationEndpoint>https://10.10.10.253:9443/dynamic-client-web/register</DynamicClientRegistrationEndpoint>
        <!--Oauth token endpoint-->
        <OauthTokenEndpoint>https://10.10.10.253:9443/oauth2/token</OauthTokenEndpoint>
        <APIS>
            <ContextPath>/services</ContextPath>
        </APIS>
    </ServerConfiguration>

## Configuring Entgra IoT Server to Install iOS Applications

The download URL for Entgra IoT Server is configured for HTTP by default. Although you can install Android mobile applications using this default configuration, to install iOS applications, you need to configure it for HTTPS as it's required for the iOS MDM protocol behavior.

Follow the steps given below to configure Entgra IoT Server to install iOS mobile applications:

1.  Open the `<IOTS_HOME>/conf/app-manager.xml` file.
2.  Add `%https%` as the value for the `AppDownloadURLHost` property.

    `<Config name="AppDownloadURLHost">%https%</Config>`





**Tip**: To test Entgra IoT Server App management features on Android devices, please use one of the following options:

*   Change the value of the `AppDownloadURLHost` property back to HTTP 
*   Continue using HTTPS to install applications on Android devices by [Generating a BKS File for Android](https://entgra-documentation.gitlab.io/v3.8.0/docs/Document/using-entgra-iot-server/Working-with-Android-Devices//#generating-a-bks-file-for-android).

## General iOS Server Configurations

This section includes the details on how to generate the Certificate Authority (CA), Registration Authority (RA) and SSL certificate. You can configure the steps using the script or you can configure it manually if you are updating your production environment. 



Before you begin!



Make sure to install the iOS features before running the steps given below. For more information, see [Installing iOS Features](/doc/en/lb2/Installing-iOS-Features.html).





### Configuring iOS server-side configurations using the script

This script automatically configures the iOS server-side configurations. This method is recommended because manually configuring the server-side configurations include many steps and if you miss out on a step you will run into errors.

Follow the steps given below:

1.  Navigate inside the `<IOTS_HOME>/ios-configurator` directory and run the `ios.sh`script. 

    cd ios-configurator
    sh ios.sh

2.  Enter the requested information when prompted. Be sure to provide the same information when configuring the [iOS platform configurations](https://docs.wso2.com/display/IoTS310/Enrolling+Without+the+iOS+Agent#EnrollingWithouttheiOSAgent-ConfiguringtheiOSplatform).

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <thead>
        <tr>
          <th>
            
          </th>
          <th>
            
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Country</td>
          <td>
            
              <p>The name of your country.</p>
              
                
                  <p><span>Enter the two digit code for your country.</span></p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>State</td>
          <td>The state your organization is at.</td>
        </tr>
        <tr>
          <td>Location</td>
          <td>The city your organization is located at.</td>
        </tr>
        <tr>
          <td>
            <p>Organization</p>
          </td>
          <td>
            <p>The name of your organization. For this scenario, we entered MobX.</p>
          </td>
        </tr>
        <tr>
          <td>Organization Unit</td>
          <td>Defined the Team ID as the organization unit.</td>
        </tr>
        <tr>
          <td>
            <p>Email</p>
          </td>
          <td>
            <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a class="external-link" href="mailto:chris@mobx.com" rel="nofollow">chris@mobx.com</a>&nbsp;as the email.</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Commonname</p>
          </td>
          <td>
            <p>Fully qualified domain name of your server.</p>
          </td>
        </tr>
      </tbody>
    </table>

    ![image](352824230.png)This part of the script creates a key pair, generates a signature, and 
    signs the key using the signature.





Make sure the `<EnabledUpdateApi>` parameter in the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file is set to `false` if you need to restart the server many times after configuring the IP.  This configuration is enabled when you run the `ios.sh` script.

By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.





### Configuring iOS server-side configurations manually



Before you begin!



Download and install OpenSSL.





For more information, see how to [download and install OpenSSL](https://www.openssl.org/source/).









Follow the instructions below to configure the iOS server-side configurations:

1.  Create a new file named `openssl.cnf` in a preferred location.

2.  Include the following configurations to the `openssl.cnf` file, to generate version 3 certificates as shown below:

    [ v3_req ] 
    # Extensions to add to a certificate request 
    basicConstraints=CA:TRUE 
    keyUsage = digitalSignature, keyEncipherment 

    [ v3_ca ] 
    # Extensions for a typical CA 
    # PKIX recommendation. 
    subjectKeyIdentifier=hash 
    authorityKeyIdentifier=keyid:always,issuer 
    # This is what PKIX recommends but some broken software chokes on critical 
    # extensions. 
    basicConstraints = critical,CA:true 
    # So we do this instead. 
    #basicConstraints = CA:true 
    # Key usage: this is typical for a CA certificate. However since it will 
    # prevent it being used as an test self-signed certificate it is best 
    # left out by default. 
    keyUsage = digitalSignature, keyCertSign, cRLSign

    

    Why is this step required?

    

    The CA, RA, and SSL certificates will be generated using the openSSL toolkit as explained in [step 4](https://entgra-documentation.gitlab.io/v3.7.0/docs/working-with-ios/ios-configurations.html#general-ios-server-configurations-Step4), [step 5](https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=28278872#GeneraliOSServerConfigurations-Step5) and [step 6](https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=28278872#GeneraliOSServerConfigurations-Step6) respectively. Therefore, the `openssl.cnf` file must be configured as explained above.

    

    

3.  In the location where you modified and saved the `openssl.cnf` file, run the following commands to generate a self-signed Certificate Authority (CA) certificate (version 3) and convert the certificate to the`.pem` format: 

    1.  `openssl genrsa -out <CA PRIVATE KEY> 4096`  
        For example: `openssl genrsa -out ca_private.key 4096`
    2.  `openssl req -new -key <CA PRIVATE KEY> -out <CA CSR>`  
        For example: `openssl req -new -key ca_private.key -out ca.csr`
    3.  `openssl x509 -req -days <DAYS> -in <CA CSR> -signkey <CA PRIVATE KEY> -out <CA CRT> -extensions v3_ca -extfile <PATH-TO-THE-NEWLY-CREATED-openssl.cnf-FILE>`  
        For example: `openssl x509 -req -days 365 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile ./openssl.cnf`
    4.  `openssl rsa -in <CA PRIVATE KEY> -text > <CA PRIVATE PEM>`  
        For example:  `openssl rsa -in ca_private.key -text > ca_private.pem`
    5.  `openssl x509 -in <CA CRT> -out <CA CERT PEM>`  
        For example: `openssl x509 -in ca.crt -out ca_cert.pem`
4.  In the same location, run the following commands to generate a Registration Authority (RA) certificate (version 3), sign it with the CA, and convert the certificate to the `.pem` format.  

    1.  `openssl genrsa -out <RA PRIVATE KEY> 4096`  
        For example:  `openssl genrsa -out ra_private.key 4096`

    2.  `openssl req -new -key <RA PRIVATE KEY> -out <RA CSR>`  
        For example: `openssl req -new -key ra_private.key -out ra.csr`
    3.  `openssl x509 -req -days <DAYS> -in <RA CSR> -CA <CA CRT> -CAkey <CA PRIVATE KEY> -set_serial <SERIAL NO> -out <RA CRT> -extensions v3_req -extfile <PATH-TO-THE-NEWLY-CREATED-openssl.cnf-FILE>`  
        For example: `openssl x509 -req -days 365 -in ra.csr -CA ca.crt -CAkey ca_private.key -set_serial 02 -out ra.crt -extensions v3_req -extfile ./openssl.cnf`
    4.  `openssl rsa -in <CA PRIVATE KEY> -text> <RA PRIVATE PEM>`  
        For example: `openssl rsa -in ra_private.key -text > ra_private.pem`
    5.  `openssl x509 -in <RA CRT> -out <RA CERT PEM>`  
        For example: `openssl x509 -in ra.crt -out ra_cert.pem`
5.  Generate the SSL certificate (version 3) based on your domain/IP address:

    

    

    If you have already obtained an SSL certificate for your domain, you can skip this step and use that SSL certificate in step 7.

    

    

    

    

    You must add your IP address/domain as the Common Name. Otherwise, provisioning will fail. 

    

    

    1.  Generate an RSA key.  
        `openssl genrsa -out <RSA_key>.key 4096`  
        For example:  
        `openssl genrsa -out ia.key 4096`
    2.  Generate a CSR file.  
        `openssl req -new -key <RSA_key>.key -out <CSR>.csr`  
        For example:  
        `openssl req -new -key ia.key -out ia.csr`  
        Enter your server IP address/domain name (e.g., 192.168.1.157) as the Common Name else provisioning will fail.
    3.  Generate the SSL certificate  
        `openssl x509 -req -days 730 -in <CSR>.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial <serial number> -out ia.crt`  
        For example:   
        `openssl x509 -req -days 730 -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt`
6.  Export the SSL, CA and RA files as PKCS12 files with an alias.

    1.  Export the SSL file as a PKCS12 file with "`wso2carbo`n" as the alias. 

        

        

        If you are using intermediate certifications, make sure to create a single certificate file that includes all these certificates by archiving them using the `cat <CERTIFCATE 1> <CERTIFICATE 2> ... >> <CERTIFICATE CHAIN>` command. Use the generated certificate chain for the proceeding step.

        

        

        `openssl pkcs12 -export -out <KEYSTORE>.p12 -inkey <RSA_key>.key -in ia.crt -CAfile ca_cert.pem -name "<alias>"`  
        For example:  
        `openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name "wso2carbon"`

    2.  Export the CA file as a PKCS12 file with an alias.  
        `openssl pkcs12 -export -out <CA>.p12 -inkey <CA private key>.pem -in <CA Cert>.pem -name "<alias>"`  
        For example:  
        `openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -name "cacert"`  
        In the above example, `cacert` has been used as the CA alias. 
    3.  Export the RA file as a PKCS12 file with an alias.  
        `openssl pkcs12 -export -out <RA>.p12 -inkey <RA private key>.pem -in <RA Cert>.pem -chain -CAfile <CA cert>.pem -name "<alias>"`  
        For example:  
        `openssl pkcs12 -export -out ra.p12 -inkey ra_private.pem -in ra_cert.pem -chain -CAfile ca_cert.pem -name "racert"`  
        In the above example, `racert` has been used as the RA alias. 

    

    Why is this step required?

    

    A PKCS12 file is used to store many cryptography objects as a single file. The certificates and their private keys that were generated using the above commands are stored in a PKCS12 file so that it can be imported to the respective KeyStores as shown in [step 9](https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=28278872#GeneraliOSServerConfigurations-Step9).

    

    

7.  Copy the three P12 extension files to the `<IoT_HOME>/repository/resources/security` directory.

    

    Why is this step required?

    

    The `<IoT_HOME>/repository/resources/security` directory is where the Entgra IoT KeyStores are stored.  
    Example for KeyStores: `wso2carbon.jks`, `client-truststore.jks` and `wso2certs.jks.`

    

    

8.  Import the generated P12 extension files as follows:  

    1.  Import the generated `<KEYSTORE>.p12` file into the `wso2carbon.jks` and `client-truststore.jks` in the` <IoT_HOME>/repository/resources/security` directory.  
        `keytool -importkeystore -srckeystore <KEYSTORE>.p12 -srcstoretype PKCS12 -destkeystore <wso2carbon.jks/client-truststore.jks>`

        

        

        *   When prompted, enter the keystore password and keystore key password as `wso2carbon`.
        *   When prompted to replace an existing entry that has the same name as wso2carbon, enter `yes`.  
            Example: `Existing entry alias wso2carbon exists, overwrite? [no]:  yes`

        

        

        For example:  
        `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore wso2carbon.jks`  
        `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore client-truststore.jks`

    2.  Import the generated `<CA>.p12` and `<RA>.p12` files into the `wso2certs.jks` file, which is in the `<IoT_HOME>/repository/resources/security` directory.  
        `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks `  

        For example:  
        `keytool -importkeystore -srckeystore ca.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks `  
        Enter the keystore password as `wso2carbon` and the keystore key password as `cacert`.

        `keytool -importkeystore -srckeystore ra.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks `  
        Enter the keystore password as  `wso2carbon`  and the keystore key password as  ``racert`` .

        

        Troubleshooting

        

        ##### Why does the following error occur: `"` `keytool error: java.io.IOException: Invalid keystore format"`?

        If you enter the wrong private key password when importing the `<CA>.p12` or `<RA>.p12` files, the `wso2certs.jks` file will get corrupted and the above error message will appear.

        In such a situation, delete the `wso2certs.jks` file and execute the following command to import the generated `<CA>.p12` and `<RA>.p12` files into the `wso2certs.jks` file again.  
        `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`  

        When the above command is executed, Entgra IoT will automatically create a new `wso2certs.jks` file with the imported file.

        

        

9.  The default IoT keystore details are defined under the `<CertificateKeystore>` XML element in the `certificate-config.xml` file, which is in the `<IoTS_HOME>/conf` directory. Therefore, if any of the following details are changed, it needs to be reflected in `<` `CertificateKeystore>`:

    *   Certificate Keystore file location
    *   Certificate Keystore type
    *   Certificate Keystore password
    *   Certificate authority certificate alias 
    *   Certificate authority private key password
    *   Registration authority certificate alias
    *   Registration authority private key password 

    Example:

    <?xml version="1.0" encoding="ISO-8859-1"?>
    <CertificateConfigurations>
        <CertificateKeystore>
            <!-- Certificate Keystore file location-->
            <CertificateKeystoreLocation>${carbon.home}/repository/resources/security/wso2certs.jks</CertificateKeystoreLocation>
            <!-- Certificate Keystore type (JKS/PKCS12 etc.)-->
            <CertificateKeystoreType>JKS</CertificateKeystoreType>
            <!-- Certificate Keystore password-->
            <CertificateKeystorePassword>wso2carbon</CertificateKeystorePassword>
            <!-- Certificate authority certificate alias -->
            <CACertAlias>cacert</CACertAlias>
            <!-- Certificate authority private key password -->
            <CAPrivateKeyPassword>cacert</CAPrivateKeyPassword>
            <!-- Registration authority certificate alias -->
            <RACertAlias>racert</RACertAlias>
            <!-- Registration authority private key password -->
            <RAPrivateKeyPassword>racert</RAPrivateKeyPassword>
        </CertificateKeystore>
        <!-- Certificate Mgt DB schema -->
        <ManagementRepository>
            <DataSourceConfiguration>
                <JndiLookupDefinition>
                    <Name>jdbc/DM_DS</Name>
                </JndiLookupDefinition>
            </DataSourceConfiguration>
        </ManagementRepository>
        <!-- Default page size of GET certificates API -->
        <DefaultPageSize>10</DefaultPageSize>
    </CertificateConfigurations>