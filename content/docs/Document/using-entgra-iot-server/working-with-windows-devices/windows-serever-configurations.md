# Windows Server Configurations

# Using Apache2 HTTP Server as the Proxy Server

Any preferred server can be used as a proxy server between the device and the Entgra IoT server. The steps documented below is only an example of configuring the proxy server by using the Apache2 HTTP Server. The Apache Server can be configured using the forward or reverse proxy (also known as gateway) mode. The reverse proxy mode is used to configure Apache2\. You can download the Apache2 HTTP Server from [here](https://httpd.apache.org/download.cgi).

Follow the steps given below to configure the proxy Server:

## Step 1: Configure reverse proxy

A reverse proxy (or gateway) appears to the client like an ordinary web server with no special configuration required for the client. Ordinary requests for content is made by the client through the `name-space`. The reverse proxy redirects the requests, and returns the required output.



The following modules are required to configure the reverse proxy:

*   `mod_proxy.so`  
    This module deals with proxying in Apache.

*   `mod_proxy_http.so` This module handles connections with both the HTTP and HTTPS protocols.

1.  Navigate to the `etc/apache2` directory and use the following command to enable the above modules:

    
    cd /etc/apache2
    a2enmod proxy_http
    

2.  Configure the `proxy.conf` file that is in the `/etc/apache2/mods-available` directory by including the configurations given below to the end of the file.

    
    ServerName localhost
    ProxyRequests off
    ProxyPreserveHost off
      <Proxy *>
      Order deny,allow
      #Deny from all
      Allow from all    
      </Proxy>
    ProxyPass /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep
    ProxyPassReverse /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep

    ProxyPass /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep
    ProxyPassReverse /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep

    ProxyPass /Syncml/initialquery http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request
    ProxyPassReverse /Syncml/initialquery http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request

    ProxyPass /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment
    ProxyPassReverse /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment

    ProxyPass /devicemgt  http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations
    ProxyPassReverse /devicemgt  http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations

    ProxyPass /windows-web-agent http://server-ip>:<server-port>/windows-web-agent
    ProxyPassReverse /windows-web-agent http://server-ip>:<server-port>/windows-web-agent
    

    

    

    The default `<server-ip>:<server-port>` is `localhost:9443.`

    

    



## Step 2: Configure the Rewrite engine







The first `GET` and `POST` HTTP requests are received by the same MDM endpoint and the rewrite conditions filter the device requests. By default the Apache Rewrite engine is disabled.





Follow the steps given below to enable the Rewrite engine when running on Ubuntu:

1.  Invoke the rewrite rules:  

    1.  Create a `.htaccess` file in the `/var/www/` directory with the specific rewrite rules.
    2.  Enable the `mod_rewrite` module.

        `sudo a2enmod rewrite`

2.  Configure the `000-default` file, which is in the `/etc/apache2/sites-enabled` directory.

    

    

    This step is required to replicate the configuration changes required in the Apache versions on a few files.In the older Apache versions, all virtual host directory directives were managed in the `apache2.conf` file, which is in the `/etc/apache2` directory. In the Apache 2.4.7 version this has changed and the alterations are handled within the `/etc/apache2/sites-enabled` directory.

    

    

    1.  Configure the value assigned to `AllowOveride` from `None` to `All` under `<Directory/>`.

        `AllowOverride All`

    2.  Configure the content under `<Directory /var/www/>`.

        

        

        If the `000-default` file does not contain the `Directory` tag you need to add it to the file as shown below:

        

        

        
        <Directory /var/www/>
         Options Indexes FollowSymLinks MultiViews
         AllowOverride all
         Order allow,deny
         allow from all
        </Directory>
        

3.  Restart the Apache server.

    `server.sudo service apache2 restart`







**Rewrite Engine Configuration file format**




RewriteEngine on
RewriteCond %{REQUEST_METHOD} ^(GET)$
RewriteRule /EnrollmentServer/Discovery.svc   http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/get [P,L]

RewriteCond %{REQUEST_METHOD} ^(POST)$
RewriteRule /EnrollmentServer/Discovery.svc http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/post [P,L]






The default `<server-ip>:<server-port>` is `localhost:9443.`













## Step 3: SSL configurations for Apache2







An SSL certificate is used to encrypt the information of a site and create a secure connection.





Follow the steps given below to configure SSL for Apache2:





SSL support is available as a standard on the Ubuntu 14.04 Apache package.





1.  Enable the SSL Module.

    `sudo a2enmod ssl`

2.  Create a subdirectory named `ssl` within the Apache server configuration hierarchy to place the certificate files.

    `sudo mkdir /etc/apache2/ssl`

    

    

    The Entgra IoTS certificate must be generated from a trusted authority.

    

    

    

    

    Once you have the Entgra IoT certificate and key available, configure the Apache server to use these files in a virtual host file. For more information, see[ how to set up Apache virtual hosts](https://www.digitalocean.com/community/articles/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts).

    

    

3.  Configure the `default-ssl.conf` file, which is in the `/etc/apache2/sites-enabled` directory contains the default SSL configurations.

    
    SSLEngine on
    SSLCertificateFile    /etc/apache2/ssl/<COMAPNY_CERTIFICATE>
    SSLCertificateKeyFile /etc/apache2/ssl/<COMPANY_PUBLIC_KEY>
    SSLCACertificateFile /etc/apache2/ssl/<COMPANY_ROOT_CERTIFICATE>
    

    Example:


    SSLEngine on
    SSLCertificateFile    /etc/apache2/ssl/star_wso2_com.crt
    SSLCertificateKeyFile /etc/apache2/ssl/star_wso2_com.key
    SSLCACertificateFile /etc/apache2/ssl/DigiCertCA.crt
    

4.  Enable the SSL-enabled virtual host that you configured in the above step.

    `sudo a2ensite default-ssl.conf`

5.  Restart the Apache server to load the new virtual host file.

    `sudo service apache2 restart`

# Using NGINX as the Proxy Server

A preferred server can be used as a proxy server between the device and the Entgra IoT server. The steps documented below is only an example of configuring the proxy server using NGINX, which is a known reverse proxy server. 

Follow the steps given below to configure the proxy Server:

1.  Install NGINX in your production environment.  
    For example, refer the following to install NGINX on a MAC or Ubuntu environment.  

2.  Get an SSL certificate. Make sure that the common name of the certificate you are getting matches the constructed URI.

    

    

    The Entgra IoTS client sends requests to the Entgra IoTS server through a Proxy Server. The Windows Entgra IoTS protocol [constructs a URI](https://docs.wso2.com/display/IoTS300/Windows+Device+Enrollment+Process+Message+Flow#WindowsDeviceEnrollmentProcessMessageFlow-Step1) that uses the hostname by appending the domain of the email address to the subdomain, `enterpriseenrollment` for each device request. Therefore, you can either purchase a domain name or create a DNS entry in the > format.

    

    

3.  Navigate to the `/usr/local/etc/nginx` directory, create a folder named `ssl`, and add the CA certificate and the private key to this folder.
4.  Configure the `/usr/local/etc/nginx/nginx.conf` file with the details of the SSL certificate and the Windows endpoints as explained below.

    1.  Compare the sample configuration file given below with your `nginx.conf` file and add the missing properties.

        

        

        

        

        

        

        What's given below is only an example. Compare your configuration file with what's given below and add the missing configurations or uncomment the commented configurations in the `nginx.conf` file.

        

        

        
        #user  nobody;
        worker_processes  1;
        #error_log  logs/error.log;
        #error_log  logs/error.log  notice;
        #error_log  logs/error.log  info;
        #pid        logs/nginx.pid;
        events {
        worker_connections  1024;
        }
        http {
        include       mime.types;
        default_type  application/octet-stream;
        #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
        #                  '$status $body_bytes_sent "$http_referer" '
        #                  '"$http_user_agent" "$http_x_forwarded_for"';
        #access_log  logs/access.log  main;
        sendfile        on;
        #tcp_nopush     on;
        #keepalive_timeout  0;
        keepalive_timeout  65;
        #gzip  on;
        server {
        listen       8080;
        server_name  localhost;
        #charset koi8-r;
        #access_log  logs/host.access.log  main;
        location / {
        root   html;
        index  index.html index.htm;
        }
        #error_page  404              /404.html;
        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
        root   html;
        }
        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}
        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}
        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
        }
        # another virtual host using mix of IP-, name-, and port-based configuration
        #
        #server {
        #    listen       8000;
        # listen       enterpriseenrollment.dilan.me;
        #    server_name  somename  alias  another.alias;
        #    location / {
        #        root   html;
        #        index  index.html index.htm;
        #    }
        #}
        # HTTPS server
        #
        #server {
        #    listen       443 ssl;
        #    server_name  localhost;
        #    ssl_certificate      cert.pem;
        #    ssl_certificate_key  cert.key;
        #    ssl_session_cache    shared:SSL:1m;
        #    ssl_session_timeout  5m;
        #    ssl_ciphers  HIGH:!aNULL:!MD5;
        #    ssl_prefer_server_ciphers  on;
        #    location / {
        #        root   html;
        #        index  index.html index.htm;
        #    }
        #}
        server {
        listen 443 ssl;
        server_name enterpriseenrollment.dilan.me;
        ssl on;
        ssl_certificate /usr/local/etc/nginx/ssl/certificate.crt;
        ssl_certificate_key /usr/local/etc/nginx/ssl/private.key;
        location /EnrollmentServer/Discovery.svc {
        if ($request_method = GET) {
        return 200;
        }
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/discovery/post;
        proxy_http_version 1.1;
        }
        location /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/certificatepolicy/xcep/1.0.0;
        proxy_http_version 1.1;
        }
        location /windows-web-agent {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:9763/windows-web-agent;

        proxy_http_version 1.1;
        }
        location /ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/deviceenrolment/wstep/;
        proxy_http_version 1.1;
        }
        location /ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/deviceenrolment/enrollment;

        proxy_http_version 1.1;
        }
        location /Syncml/initialquery {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/syncml/devicemanagement/1.0.0/request/;
        proxy_http_version 1.1;
        }
        location /devicemgt {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.8.100:8280/api/device-mgt/windows/v1.0/management/devicemgt/1.0.0/pending-operations/;
        proxy_http_version 1.1;
        }
        }
        include servers/*;
        }
        

        

        

        

    2.  Configure the SSL certificate details.

        
        server {
                listen 443;
                server_name enterpriseenrollment.wso2.com;
                ssl on;
                ssl_certificate /usr/local/etc/nginx/ssl/star_wso2_com.crt;
                ssl_certificate_key /usr/local/etc/nginx/ssl/enterpriseenrollment_wso2_com.key;
        

        You need to configure the following properties:

        <table>
          <colgroup>
            <col>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>Property</th>
              <th>Description</th>
              <th>Example</th>
            </tr>
            <tr>
              <td>
                <p><code>server_name</code></p>
              </td>
              <td>Define the common name of the certificate.</td>
              <td>
                <p><code>enterpriseenrollment.wso2.com</code></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><code>ssl_certificate</code></p>
              </td>
              <td>Define where you saved the SSL certificate.</td>
              <td>
                <p><code>/usr/local/etc/nginx/ssl/wso2_com_SSL.crt</code></p>
              </td>
            </tr>
            <tr>
              <td>
                
              </td>
              <td>Define where you saved the private key of the certificate.</td>
              <td>
                <p><code>/usr/local/etc/nginx/ssl/enterpriseenrollment_wso2_com.key</code></p>
              </td>
            </tr>
          </tbody>
        </table>

    3.  Configure the Windows endpoints.

        

        

        

        

        *   The `GATEWAY_PORT` used by default in Entgra IoTS for device management is 8280.
        *   For more information on how the Windows endpoints are used, see [how the Windows device enrollment process message flow works](/doc/en/lb2/Windows-Device-Enrollment-Process-Message-Flow.html).

        

        

        

        

        

        

        

        Example:

        location /ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc {  
            proxy_set_header X-Forwarded-Host $host:$server_port;
            proxy_set_header X-Forwarded-Server $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            proxy_pass http://10.10.10.10:8280/api/device-mgt/windows/v1.0/certificatepolicy/xcep;

            proxy_http_version 1.1;
        }

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>Property</th>
              <th>Description</th>
            </tr>
            <tr>
              <td><code>location</code></td>
              <td>This property specifies the "<code><strong>/"</strong></code><strong> </strong>prefix that needs to be compared with the URI sent from the request. For more information, see the <a href="http://nginx.org/en/docs/beginners_guide.html#conf_structure" class="external-link" rel="nofollow">NGINX documentation</a>.</td>
            </tr>
            <tr>
              <td>
                <p><code>proxy_set_header</code></p>
              </td>
              <td>Required to configure Windows for reverse proxy.</td>
            </tr>
            <tr>
              <td><code>proxy_pass</code></td>
              <td>Define the respective Windows endpoint.</td>
            </tr>
          </tbody>
        </table>

# Windows Device Enrollment Process Message Flow

## Overview

Windows devices are enrolled through the inbuilt application of the respective device, i.e Company app on Windows 8.0 , Workplace on Windows 8.1 and Work Access on Windows 10\. The endpoints it triggers cannot be altered as required by the service developer. Therefore additional server configurations are required to manage Windows devices using Entgra IoTS.





For more information, see [Windows Server Configurations](/doc/en/lb2/Windows-Server-Configurations.html).





The high level message flow of a Windows device is shown in the below diagram. The Windows device sends requests to a Proxy server that then directs the requests to the Entgra IoT Server. 

![image](352824771.png)

## How it works

#### Step 1: Initiate the enrollment process

*   When a user signs in through the Workplace application using a Windows device, the user's email address needs to be provided as shown below:

    

    

    The email address is provided in the following format: `<user>@<EMAIL_DOMAIN>`.

    

    

*   The automatic discovery service of the device uses the following fields and constructs a unique URI:
    *   Append the subdomain `enterpriseenrollment`.
    *   Extracts and append the domain from the username, i.e `<EMAIL_DOMAIN>`, that was submitted when signing in.
    *   Append the path `/ENROLLMENTSERVER/Discovery.svc`.

    

    

    The constructed URI will take the following format: `enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/Discovery.svc`

    Example: If the given user name is `admin@wso2.com` the resulting URI would be `enterpriseenrollment.wso2.com/ENROLLMENTSERVER/Discovery.svc`.

    

    

*   The constructed URI will then be used to initiate the enrollment process of the mobile device.

### Step 2: Proxy endpoint 01

**URI format**


http://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/discovery.svc


*   The first Proxy endpoint known as the `Discovery Service Endpoint` receives the initial request from the device. 

    

    

    The initial request from the device is a standard `HTTP GET` request.

    

    

*   The received request is redirected by the Proxy Server to MDM endpoint 01 at the server end.

    

    

    The `GET` request helps the device to check if the server is up and running.

    

    

*   Once a success message is returned to the device by MDM endpoint 01, It will move to the next step.

#### Step 3: MDM endpoint 01

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/get `

*   This endpoint receives the initial `GET` request that was redirected by the Proxy Server. It then returns a success response to the device. 
*   When the endpoint receives the success message from the server, the device triggers a `HTTPS POST` request to the same proxy endpoint (Proxy endpoint 01) .  
    The Proxy Server redirects the request to MDM endpoint 02, which is at the server side.

#### Step 4: MDM endpoint 02

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/post `

*   Once the `POST` request from the device is received at this endpoint, the MDM server will include the following details within a response body and send to the device. 
*   If Requested user device is Windows 8/8.1 one, user gets 3.a proxy endpoints for the response of the POST request, But If its a Windows 10 Device, He/She gets 3.b proxy endpoint.  
    1\. Authentication Policy.(Federated)  
    2\. Proxy endpoint for the Enrollment policy. (`/ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc`) - Proxy endpoint 02.  
    3\. a. Proxy endpoint for the Enrollment Service. (`/ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc`) - Proxy endpoint 03 a  

        b. Proxy endpoint for the Enrollment Service. (`/ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc`) - Proxy endpoint 03 b

    4\. Proxy endpoint for the Windows login page. (`/windows-web-agent`) - Proxy endpoint for the federated login page.

     First of all device receive proxy end point for federated login page as following:

`https://enterpriseenrollment.<EMAIL_DOMAIN>/emm/enrollments/windows/login-agent`

*   After receiving the above responses, the device requests are made in the following order:
    1.  The device requests for the login page using the proxy endpoint URL. The proxy server will then route the request to the following IoTS endpoint.

        `http://<server-ip>:<server-port>/windows-web-agent`

    2.  Next the device sends a request to Proxy endpoint 2 and 3 respectively.

#### Step 5: Proxy endpoint 02

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/policyenrollmentwebservice.svc `

This Proxy Server routes the messages to MDM endpoint 03.

#### Step 6: MDM endpoint 03

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep`

*   MDM Endpoint 3 responds back to the device with the Certificate Enrollment Policy.

    

    

    The Certificate Enrollment Policy is an implementation of the MS-XCEP 509 protocol.

    

    

    Once the certificate enrollment policy is received, the device sends the Certificate Signing Request (CSR) to **Proxy endpoint 03**.

#### **Step 7: Proxy endpoint 03\. a**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/deviceenrollmentwebservice.svc `

This Proxy Server routes the message received from MDM endpoint 03 a. to **MDM endpoint 04.a**

#### **Step 8: Proxy endpoint 03\. b**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/Win10deviceenrollmentwebservice.svc `

This Proxy Server routes the message received from MDM endpoint 03 b. to **MDM endpoint 04.b**

#### Step 8: MDM Endpoint 04\. a

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep`

*   The service corresponding to this endpoint generates the signed certificate for the Certificate Signing Request and provide next enrollment Proxy endpoint **Proxy Endpoint 4.a**
*   It then responds to the device with an encoded `wap-provisioning.xml` file, which includes necessary certificates and other Device Management information.





The certificate enrollment is an implementation of the `MS-WSTEP` protocol.

The initial details provided through the `POST` request are used to persist the device details in the database.





#### Step 8: MDM Endpoint 04\. b

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment`

*   The service corresponding to this endpoint generates the signed Certificate for the Certificate Signing Request and Enroll the Windows 10 devices against the IOT server and provide the get pending Proxy endpoint **Proxy Endpoint 4.b**

#### Step 9: Proxy Endpoint 04\. a

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/syncml/initialquery`

This Proxy Server routes the message received to **MDM endpoint 05**.

At the end of this flow Device is enrolled successfully.

#### **Step 9: Proxy Endpoint 04\. b**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/devicemgt`

This Proxy Server routes the message received to **MDM endpoint 06**.

#### **Step 10: MDM Endpoint 05**

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request`

#### **Step 10: MDM Endpoint 06**

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations`

This endpoint will handle all the device management requests and responses. 





The protocol that is being used is SyncML v1.2.
